#include "../utilitarios/utilitarios.hpp"
vector<pair<string,vector<string> > > filtros3;
vector<pair<string,string> > seleccionados;
vector<vector<string> > vectorResultados;
vector<vector<string> > carrito;
vector<vector<string> > comparados;
int contComparados=0;

void mostrarOp_v3(int pos,string opcion) {
	cout<<pos<<". "<<opcion<<" : ";
}

void cargarFiltros3() {
	ifstream archivo;
	archivo.open(datosFiltros.c_str());
	if (archivo.is_open()==false) {
		error();
	}
	else {
		string linea1;
		while (getline(archivo,linea1)) {
			string linea2;
			stringstream aux1(linea1);
			getline(aux1,linea2,',');
			pair<string,string> parAux1;
			pair<string,vector<string> > parAux2;
			parAux1.first=linea2;
			parAux1.second="-";
			parAux2.first=linea2;
			vector<string> vectorAux;
			while (getline(aux1,linea2,',')) {
				vectorAux.push_back(linea2);
			}
			parAux2.second=vectorAux;
			seleccionados.push_back(parAux1);
			filtros3.push_back(parAux2);
		}
	}
	archivo.close();
}

void mostrarSeleccionados3() {
	int longVector=seleccionados.size();
	cout<<longVector<<endl;
	for (int i=0;i<longVector;i++) {
		cout<<seleccionados[i].first<<"   "<<seleccionados[i].second<<endl;
	}
}

void mostrarFiltros3() {
	int longVector=filtros3.size();
	for (int i=0;i<longVector;i++) {
		cout<<filtros3[i].first<<"   ";
		int longVector1=filtros3[i].second.size();
		for (int j=0;j<longVector1;j++) {
			cout<<(filtros3[i].second)[j]<<" ";
		}
		cout<<endl;
	}
}

void menuAnterior3(int pos) {
	mostrarOp(pos,"Volver al Menu Anterior"); linea();
}

void recibir(int pos1,int pos2) {
	int opcion1;
	for (int i=0;i<pos2-pos1+1;i++) {
		mostrarOp(i+1,filtros3[i+pos1-1].first);
	}
	menuAnterior3(pos2-pos1+2);
	introducir(opcion1,pos2-pos1+2);
	if (opcion1!=pos2-pos1+2) {
		int longitud=filtros3[opcion1+pos1-2].second.size();
		for (int i=0;i<longitud;i++) {
			mostrarOp(i+1,filtros3[opcion1+pos1-2].second[i]);
		}
		menuAnterior3(longitud+1);
		int opcion2;
		introducir(opcion2,longitud+1);
		if (opcion2!=longitud+1) {
			seleccionados[opcion1+pos1-2].second=filtros3[opcion1+pos1-2].second[opcion2-1];
		}
	}
}

void categoriasSeleccionar(int &opcion1) {
	mostrarTile("Buscar por Filtros");
	mostrarOp(1," Informacion");
	mostrarOp(2," Componente");
	mostrarOp(3," Apariencia");
	mostrarOp(4," Bateria");
	mostrarOp(5," Camara");
	mostrarOp(6," Conectividad");
	mostrarOp(7," Pantalla");
	mostrarOp(8," Seguridad");
	mostrarOp(9," Software");
	mostrarOp(10,"Extras");
	mostrarOp(11,"Buscar");
	menuAnterior3(12);
}

void switchRecibirFiltros(int opcion1) {
	switch (opcion1) {
		case 1: recibir(1,1);
			break;
		case 2: recibir(5,7);
			break;
		case 3: recibir(8,12);
			break;
		case 4: recibir(13,15);
			break;
		case 5: recibir(16,19);
			break;
		case 6: recibir(20,21);
			break;
		case 7: recibir(22,24);
			break;
		case 8: recibir(25,27);
			break;
		case 9: recibir(28,28);
			break;
		case 10: recibir(29,34);
			break;
	}
}

void seleccionar3(string idComp,int num) {	//0 SALE MENU PARA ESCOGER | 1 VALORES POR DEFECTO DE UN RECOMENDADO
	vectorResultados.clear();
	int opcion1;
	if (num==0) {
		categoriasSeleccionar(opcion1);
		introducir(opcion1,12);
	}
	else if (num==1) {
		opcion1=11;
	}
	if (opcion1==11) {
		ifstream archivo;
		archivo.open(datosCelulares.c_str());
		if (archivo.is_open()==false) {
			error();
		}
		else {
			string linea1,linea2;
			while (getline(archivo,linea1)) {
				stringstream aux1(linea1);
				int cont1=0,cont2=0;
				while (getline(aux1,linea2,',')) {
					if (seleccionados[cont1].second=="-") {
						cont2++;
					}
					else if (seleccionados[cont1].second==linea2) {
						cont2++;
					}
					cont1++;
				}
				if (cont1==cont2) {
					vector<string> auxiliar;
					string linea3;
					stringstream aux2(linea1);
					while (getline(aux2,linea3,',')) {
						auxiliar.push_back(linea3);
					}
					vectorResultados.push_back(auxiliar);
				}
			}
			int longResultados=vectorResultados.size();
			mostrarTile("Resultados");
			if (longResultados==0) {
				cout<<"No se encontro coincidencias"<<endl; linea();
			}
			else {
				cout<<completarCad("",2)
					<<completarCad("Marca",10)
					<<completarCad("Modelo",15)
					<<completarCad("Precio",8)
					<<completarCad("Stock",8)
					<<completarCad("RAM",5)
					<<completarCad("ROM",5)
					<<completarCad("Procesador",12)
					<<completarCad("Color",8)<<endl;
				linea();
				for (int i=0;i<longResultados;i++) {
					cout<<INTtoSTRING(i+1)+". "
						<<completarCad(vectorResultados[i][0],10)
						<<completarCad(vectorResultados[i][1],15)
						<<completarCad(vectorResultados[i][2],8)
						<<completarCad(vectorResultados[i][3],8)
						<<completarCad(vectorResultados[i][4],5)
						<<completarCad(vectorResultados[i][5],5)
						<<completarCad(vectorResultados[i][6],12)
						<<completarCad(vectorResultados[i][7],8)<<endl;
				}
				cout<<endl;
				menuAnterior3(longResultados+1);
				int opcion2;
				introducir(opcion2,longResultados+1);
				if (opcion2!=longResultados+1) {
					int longCelular=vectorResultados[opcion2-1].size();
					mostrarTile("Detalles del Celular");
					for (int i=0;i<longCelular;i++) {
						switch (i) {
							case 0: cout<<"Informacion------------------"<<endl;
								break;
							case 4: cout<<"Componente-------------------"<<endl;
								break;
							case 7: cout<<"Apariencia-------------------"<<endl;
								break;
							case 12: cout<<"Bateria---------------------"<<endl;
								break;
							case 15: cout<<"Camara----------------------"<<endl;
								break;
							case 19: cout<<"Conectividad----------------"<<endl;
								break;
							case 21: cout<<"Pantalla--------------------"<<endl;
								break;
							case 24: cout<<"Seguridad-------------------"<<endl;
								break;
							case 27: cout<<"Software--------------------"<<endl;
								break;
							case 28: cout<<"Extras----------------------"<<endl;
								break;
						}
						cout<<"   "<<filtros3[i].first<<": "<<vectorResultados[opcion2-1][i]<<endl;
					}
					linea(); system("pause");
					mostrarTile("Acciones");
					mostrarOp(1,"Aniadirlo al Carro");
					mostrarOp(2,"Compararlo");
					menuAnterior3(3);
					int opcion3;
					introducir(opcion3,3);
					if (opcion3==1) {
						int cantidad=0;
						if (vectorResultados[opcion2-1][3]>"0") {
							mostrarTile("Cantidad");
							cout<<"Ingrese la cantidad: ";
							introducir(cantidad,STRINGtoINT(vectorResultados[opcion2-1][3]));
							mostrarTile("Confirmacion");
							mostrarOp(1,"Si");
							mostrarOp(2,"No");
							linea();
							introducir(opcion3,2);
							if (opcion3==1) {


								ofstream archivo2;
								vectorResultados[opcion2-1][3]=INTtoSTRING(cantidad);
								archivo2.open(datosCarritoTemporal.c_str(),ios::app|ios::out);
								archivo2<<idComp;
								for (int i=0;i<longCelular-1;i++) {
									archivo2<<vectorResultados[opcion2-1][i]+",";
								}
								archivo2<<vectorResultados[opcion2-1][longCelular-1]<<endl;
								cout<<"Aniadido con Exito al Carrito"<<endl;
								archivo2.close();
								system("pause");
							}
						}
						else if (vectorResultados[opcion2-1][3]=="0") {
							cout<<"No cuenta con stock"<<endl;
						}
					}
					else if (opcion3==2) {
						if (contComparados==1) {
							comparados.push_back(vectorResultados[opcion2-1]);
							system("cls");
							for (int i=0;i<longCelular;i++) {
								switch (i) {
									case 0: cout<<"Informacion--------------------------------------------"<<endl;
										break;
									case 4: cout<<"Componente---------------------------------------------"<<endl;
										break;
									case 7: cout<<"Apariencia---------------------------------------------"<<endl;
										break;
									case 12: cout<<"Bateria-----------------------------------------------"<<endl;
										break;
									case 15: cout<<"Camara------------------------------------------------"<<endl;
										break;
									case 19: cout<<"Conectividad------------------------------------------"<<endl;
										break;
									case 21: cout<<"Pantalla----------------------------------------------"<<endl;
										break;
									case 24: cout<<"Seguridad---------------------------------------------"<<endl;
										break;
									case 27: cout<<"Software----------------------------------------------"<<endl;
										break;
									case 28: cout<<"Extras------------------------------------------------"<<endl;
										break;
								}
								cout<<"   "<<completarCad(filtros3[i].first+": ",20)<<completarCad(comparados[0][i],10)<<completarCad(comparados[1][i],10)<<endl;
							}
							linea();
							mostrarOp(1,"Comprar "+comparados[0][0]+" "+comparados[0][1]);
							mostrarOp(2,"Comprar "+comparados[1][0]+" "+comparados[1][1]);
							menuAnterior3(3);
							int opcion4;
							introducir(opcion4,3);
							if (opcion4!=3) {
								int longVector=comparados[0].size();
								if (opcion4!=3) {
									ofstream archivo2;
								    archivo2<<idComp;
									archivo2.open(datosCarritoTemporal.c_str(),ios::app|ios::out);
									for (int k=0;k<longVector;k++) {
										archivo2<<comparados[opcion4-1][k];
										if (k!=longVector-1) {
											archivo2<<",";
										}
										else {
											archivo2<<endl;
										}
									}
									cout<<"Aniadido con Exito al Carrito"<<endl;
									archivo2.close();
									system("pause");
								}
							}
							contComparados=0;
						}
						else if (contComparados==0) {
							comparados.push_back(vectorResultados[opcion2-1]);
							contComparados++;
							if (num==0) {
								filtros3.clear();
								cargarFiltros3();
								seleccionados.clear();
							}
							seleccionar3(idComp,num);
						}
					}
				}
			}
		}
		archivo.close();
	}
	else if (opcion1!=11 && opcion1!=12) {
		if (num==0) {
			switchRecibirFiltros(opcion1);
		}
		seleccionar3(idComp,num);
	}
}

void buscarRecomendados(string idComp) {
	int opcion;
	mostrarTile("Smarthphone Recomendados");
	mostrarOp(1,"Los mejores celulares en Fotografia");
	mostrarOp(2,"Los celulares con la mayor duracion de uso");
	mostrarOp(3,"Los celulares con las mejores pantallas");
	mostrarOp(4,"Los celulares mas resistentes");
	menuAnterior3(5);
	introducir(opcion,5);
	if (opcion!=5) {
		seleccionados.clear();
		filtros3.clear();
		cargarFiltros3();
		switch (opcion) {
			case 1: seleccionados[18].second="Si";
					seleccionar3(idComp,1);
				break;
			case 2: seleccionados[12].second="4000";
					seleccionar3(idComp,1);
				break;
			case 3: seleccionados[21].second="QHD";
					seleccionar3(idComp,1);
				break;
			case 4: seleccionados[10].second="Gorilla Glass 5";
					seleccionados[11].second="Salpicadura";
					seleccionar3(idComp,1);
				break;
		}
	}
}

void buscar3(string idComp,int num) {
	seleccionados.clear();
	filtros3.clear();
	cargarFiltros3();
	seleccionar3(idComp,num);
	//mostrarSeleccionados3();
	//mostrarFiltros3();
}






