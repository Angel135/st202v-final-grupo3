#include "../utilitarios/utilitarios.hpp"

map<string,vector<string> > filtros;
map<string,string> select;

void mostrarOp_v2(int pos,string opcion) {
	cout<<pos<<". "<<opcion<<" : ";
}

void errorBusqueda() {
	cout<<"No hubo Coincidencias"<<endl; linea();
}

void cargarFiltros() {	//Carga los filtros
	ifstream archivo;
	archivo.open(datosFiltros.c_str());
	if (archivo.is_open()) {
		string linea1;
		while (getline(archivo,linea1,'\n')) {
			stringstream aux1(linea1);
			string linea2;
			vector<string> vectorAux;
			pair<string,string> selectAux;
			pair<string,vector<string> > filtrosAux;
			getline(aux1,linea2,',');
			selectAux.first=linea2;
			selectAux.second="-";
			filtrosAux.first=linea2;
			while (getline(aux1,linea2,',')) {
				vectorAux.push_back(linea2);
			}
			filtrosAux.second=vectorAux;
			select.insert(selectAux);
			filtros.insert(filtrosAux);
		}
	}
	else {
		error();
	}
	archivo.close();
}

void mostrarFiltros() {	//Muestra los datos de filtros.txt
	cargarFiltros();
	map<string,vector<string> >::iterator it;
	mostrarTile("Todos los Filtros");
	for (it=filtros.begin();it!=filtros.end();it++) {
		cout<<(*it).first<<": ";
		int longitud=(*it).second.size();
		for (int i=0;i<longitud;i++) {
			if (i==longitud-1) {
				cout<<(*it).second[i]<<endl;
			}
			else {
				cout<<(*it).second[i]<<" ";
			}
		}
	}
	linea();
}

void mostrarSeleccionados() {	//Muestra los filtros seleccionados
	cargarFiltros();
	map<string,string >::iterator it2;
	mostrarTile("Filtros Seleccionados");
	for (it2=select.begin();it2!=select.end();it2++) {
		cout<<(*it2).first<<": "<<(*it2).second<<endl;
	}
	linea();
}

void menuAnterior(int pos) {
	mostrarOp(pos,"Volver al Menu Anterior"); linea();
}

void seleccionarFiltros() {	//Menu para seleccionar filtros
	int opcion,opcion2;
	cargarFiltros();
	mostrarTile("Busqueda por Filtros");
	map<string,vector<string> >::iterator it;
	map<string,string>::iterator it2;
	vector<string> categoria;	//guarda las categorias
	//categorias de filtros
	int contador=0;
	it2=select.begin();
	for (it=filtros.begin();it!=filtros.end();it++) {
		contador++;
		categoria.push_back((*it).first);
		mostrarOp_v2(contador,(*it).first);
			cout<<(*it2).second<<endl;
		it2++;
	}
	mostrarOp(contador+1,"BUSCAR!");
	menuAnterior(contador+2);
	introducir(opcion,contador+2);
	//dentro de una categoria
	if (opcion!=contador+2 && opcion!=contador+1) {
		it=filtros.find(categoria[opcion-1]);
		it2=select.find(categoria[opcion-1]);
		int longitud=(*it).second.size();
		int contador2=0;
		for (;contador2<longitud;contador++) {
			contador2++;
			mostrarOp(contador2,(*it).second[contador2-1]);
		}
		menuAnterior(contador2+1);
		introducir(opcion2,contador2+1);
		if (opcion2!=contador2+1) {
			(*it2).second=(*it).second[opcion2-1];
		}
		seleccionarFiltros();
	}
}

void buscar_v2() {
	ifstream archivo;
	string linea1,linea2,linea3;
	vector<string> coincidencias;
	archivo.open(datosCelulares.c_str());
	if (archivo.is_open()) {
		map<string,string>::iterator it2;
		pair<string,string> par1;

		//getline(archivo,linea3);
		while (getline(archivo,linea1)) {
			stringstream aux1(linea1);
			it2=select.begin();
			int contador1=0;
			int contador2=0;
			while (getline(aux1,linea2,',') && it2!=select.end()) {
				par1=*it2;
				if (par1.second!="-") {
					if (par1.second==linea2) {
						it2++;
						contador1++;
					}
					else {
						it2=select.end();
					}
				}
				else {
					contador1++;
					it2++;
				}
				contador2++;
			}
			if (contador1==contador2) {
				coincidencias.push_back(linea1);
			}
		}
		int longitud=coincidencias.size();
		if (longitud!=0) {
			mostrarTile("Resultados");
			linea3="Nombre Modelo   Precio";
			cout<<linea3<<endl;
			for (int i=0;i<longitud;i++) {
				cout<<i+1<<". ";
				stringstream aux1;
				aux1<<coincidencias[i];
				for (int j=0;j<34;j++) {
					string linea4;
					getline(aux1,linea4,',');
					if (j==23 || j==24 || j==25) {
						cout<<linea4<<" ";
					}
				}
				cout<<""<<endl;
			}
			menuAnterior(longitud+1);
			int opcion3;
			introducir(opcion3,longitud+1);
			if (opcion3!=longitud+1) {
				mostrarTile("Producto Seleccionado");
				cout<<coincidencias[opcion3-1]<<endl;
			}
		}
		else {
			errorBusqueda();
		}
	}
	else {
		error();
	}
}

void mostrarResultados_v2() {

}

void busquedaProductos() {	//Funcion de busqueda
	mostrarFiltros();
	mostrarSeleccionados();
	seleccionarFiltros();
	mostrarSeleccionados();
	buscar_v2();
}

















