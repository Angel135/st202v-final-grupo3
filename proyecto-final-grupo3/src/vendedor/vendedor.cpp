#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include "../utilitarios/utilitarios.hpp"

using namespace std;

struct Vendedor{
	string nombre;
	string apellido;
	int dni;
	int ruc;
	string id;
	string contrasenia;
	string correo;

	string crearlinea(){
		string linea;
		return linea=nombre+','+apellido+','+INTtoSTRING(dni)+','+INTtoSTRING(ruc)
				+','+id+','+contrasenia+','+correo;
	}

	void registrarVendedor(){
		string linea=crearlinea();
		ofstream archivo;
		archivo.open("datos/vendedor.txt",ios::app);
		if(archivo.is_open()){
			archivo <<linea << endl;
		}else{
			cout << "El archivo no pudo abrirse" << endl;
		}
		archivo.close();

		archivo.close();
		system("pause");
	}
};

struct Producto{
	string color;
	string material;
	string notch;
	string proteccion;
	string resistencia;
	int capacidad;
	string cargaInalambrica;
	string extraible;
	int camFrontal;
	string camFrontalDual;
	int camPrincipal;
	string camPrincipalDual;
	string procesador;
	int ram;
	int rom;
	string cuatroG;
	string dobleSim;
	string infrarrojo;
	string jackAudifonos;
	string nfc;
	string radio;
	string tipoEntrada;
	string marca;
	string modelo;
	float precio;
	int stock;
	string resolucion;
	float tamanio;
	string tecnologia;
	string faceUnlock;
	string huella;
	string posHuella;
	string software;

	string crearLinea(){
			string linea;
			return linea=color+','+material+','+notch+','+proteccion+','+resistencia+','+INTtoSTRING(capacidad)+','+cargaInalambrica
					+','+extraible+','+INTtoSTRING(camFrontal)+','+camFrontalDual+','+INTtoSTRING(camPrincipal)+','+camPrincipalDual+','+procesador
					+','+INTtoSTRING(ram)+','+INTtoSTRING(rom)+','+cuatroG+','+dobleSim+','+infrarrojo+','+jackAudifonos+','+nfc
					+','+radio+','+tipoEntrada+','+marca+','+modelo+','+FLOATtoSTRING(precio)+','+INTtoSTRING(stock)+','+resolucion
					+','+FLOATtoSTRING(tamanio)+','+tecnologia+','+faceUnlock+','+huella+','+posHuella
					+','+software;
	}

	void registrarNuevoProducto(){
		string linea=crearLinea();
		ofstream archivo;
		archivo.open("../datos/celulares.txt",ios::app);
		if(archivo.is_open()){
			archivo <<linea << endl;
		}else{
			cout << "El archivo no pudo abrirse" << endl;
		}
		archivo.close();

		archivo.close();
		system("pause");
	}
};

void registrarVendedor(){
	cout << "\n\t\tBienvenido: Ud se va a registrar como un nuevo vendedor" << endl;
	vector<Vendedor> vendedor1;
	Vendedor primer;

	cout<<"Ingrese Nombre";
	getline(cin,primer.nombre);
	cout<<"Ingrese Apellido";
	getline(cin,primer.apellido);
	cout<<"Ingrese DNI";
	cin>>primer.dni;
	cout<<"Ingrese RUC";
	cin>>primer.ruc;
	cout<<"Ingrese un ID";
	getline(cin,primer.id);
	cout<<"Ingrese su contrasenia";
	getline(cin,primer.contrasenia);

	vendedor1.push_back(primer);
	primer.registrarVendedor();
	cout << "\n Ud se ha registrado con exito" << endl;
	system("pause");
}

//funciones principales del menu


void clear_stock(){
	int opcion;

	mostrarTile("Desea limipar stock");
	mostrarOp(1,"Si");
	mostrarOp(2,"No");
	cin>>opcion;
}


void consultarRegistro(){
	cout<<"Estos son los ultimos 10 productos ingresados y su stock \n";
	ifstream archivo;
	archivo.open("datos/celulares.txt");
	string linea;
	string registrocel;
	for(int i=0; i<10;i++){
		getline(archivo,registrocel,',');
		getline(archivo,registrocel,',');cout<<registrocel;
		cout<<",";
		getline(archivo,registrocel,',');
		getline(archivo,registrocel,',');cout<<registrocel;
		getline(archivo,linea);
		cout<<endl;
	}
}


void consultarVenta(){

}


void consultarProductos(){

}


void modificarDatosVendedor(){
	vector<Vendedor> vendedor1;
	Vendedor primer;
	cout<<"Ud va a modificar los datos de la cuenta, por favor llene los siguientes campos: \n";
	cout<<"Ingrese Nombre";
	getline(cin,primer.nombre);
	cout<<"Ingrese Apellido";
	getline(cin,primer.apellido);
	cout<<"Ingrese DNI";
	cin>>primer.dni;
	cout<<"Ingrese RUC";
	cin>>primer.ruc;
	cout<<"Ingrese un nuevo ID";
	getline(cin,primer.id);
	cout<<"Ingrese su nueva contrasenia";
	getline(cin,primer.contrasenia);

	vendedor1.push_back(primer);
	primer.registrarVendedor();
	cout << "\n Ud se ha cambiado sus datos con exito" << endl;
	system("pause");
}


//pantalla registrar
void pantallaRegistrar(){
	cout << "\n\t\tBienvenido a la interfaz de registro de productos" << endl;
	vector<Producto> productos;
	Producto cel;
	cout<<"APARIENCIA \n";
	cout<<"El color del celular es: ";cin>>cel.color;
	cout<<"De que material es: ";cin>>cel.material;
	cout<<"Tiene Notch (Si/No): ";cin>>cel.notch;
	cout<<"Que tipo de proteccion tiene (Gorilla3/Gorilla4/Gorilla5): ";cin>>cel.proteccion;
	cout<<"A que es resistente: ";cin>>cel.resistencia;
	cout<<"BATERIA \n";
	cin.ignore();
	cout<<"Cual es su capacidad: ";cin>>cel.capacidad;
	cout<<"Tiene carga inalambrica (Si/No): ";cin>>cel.cargaInalambrica;
	cout<<"La bateria es extraible (Si/No): ";cin>>cel.extraible;
	cout<<"CAMARA\n";
	cin.ignore();
	cout<<"Potencia de la camara frontal: ";cin>>cel.camFrontal;
	cout<<"Es dual (Si/No): ";cin>>cel.camFrontalDual;
	cin.ignore();
	cout<<"Potencia de la camara principal: ";cin>>cel.camPrincipal;
	cout<<"Es dual (Si/No): ";cin>>cel.camPrincipalDual;
	cout<<"COMPONENTE\n";
	cout<<"Que tipo de procesador tiene?: ";cin>>cel.procesador;
	cin.ignore();
	cout<<"Cuanto de RAM tiene?: ";cin>>cel.ram;
	cout<<"Cuanto de ROM tiene?: ";cin>>cel.rom;
	cout<<"CONECTIVIDAD\n";
	cout<<"Tiene acceso a la red 4G (Si/No): ";cin>>cel.cuatroG;
	cout<<"Tiene doble SIM? (Si/No): ";cin>>cel.dobleSim;
	cout<<"EXTRAS\n";
	cout<<"Tiene infrarrojo? (Si/No): ";cin>>cel.infrarrojo;
	cout<<"Tiene Jack Audifonos? (Si/No): ";cin>>cel.jackAudifonos;
	cout<<"Tiene NFC? (Si/No): ";cin>>cel.nfc;
	cout<<"Tiene radio? (Si/No): ";cin>>cel.radio;
	cout<<"Cual es el tipo de entrada? (MicroUSB/TipoC/Lightning): ";cin>>cel.tipoEntrada;
	cout<<"INFORMACION\n";
	cout<<"Cual es la marca del celular?: ";cin>>cel.marca;
	cout<<"Cual es el modelo del celular?: ";cin>>cel.modelo;
	cin.ignore();
	cout<<"Cual es el precio?: ";cin>>cel.precio;
	cout<<"Cuanto STOCK tiene disponible: ";cin>>cel.stock;
	cout<<"PANTALLA \n";
	cout<<"Cual es la resolucion de la pantalla?: ";cin>>cel.resolucion;
	cin.ignore();
	cout<<"Cual es el tamanio de la pantalla?: ";cin>>cel.tamanio;
	cout<<"Que tecnologia es? (IPS/Oled): ";cin>>cel.tecnologia;
	cout<<"SEGURIDAD \n";
	cout<<"Tiene desbloqueador por reconocimiento facial? (Si/No): ";cin>>cel.faceUnlock;
	cout<<"Tiene desbloqueador por medio de huella (Si/No): ";cin>>cel.huella;
	cout<<"Donde? (Frontal/Trasera): ";cin>>cel.posHuella;
	cout<<"SOFTWARE \n";
	cout<<"Que tipo de software utiliza?: ";cin>>cel.software;

	productos.push_back(cel);
	cel.registrarNuevoProducto();
	cout << "\nProducto ingresado con exito" << endl;
	system("pause");

}

//Menu vendedor
void menuVendedor(){
	int n;

	cout<<"Bienvenido "<<endl;
	cout<<"1.- Registrar nuevo producto \n";
	cout<<"2.- Consultar registro de productos \n";//Lista de productos nuevos
	cout<<"3.- Consultar ventas \n";//Lista de productos vendidos
	cout<<"4.- Consultar productos \n";//Lista productos disponibles(stock)
	cout<<"5.- Modificar sus datos \n";//

	cout<<"Ingrese opcion: \n";
	cin>>n;

	switch (n){
	case 1:
		pantallaRegistrar();
			break;
	case 2:
		consultarRegistro();
			break;
	case 3:
		consultarVenta();
			break;
	case 4:
		consultarProductos();
			break;
	case 5:
		modificarDatosVendedor();
			break;
	default:
		cout<<"Opcion invalida";
		menuVendedor();
	}
}
