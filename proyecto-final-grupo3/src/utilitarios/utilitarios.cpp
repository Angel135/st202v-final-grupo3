#include <iostream>
#include <sstream>
#include <cstdlib>
using namespace std;

void linea() {
	cout<<"-------------------------------------------------------------------------------"<<endl;
}
void lineaCorta() {
	cout<<"---------------------"<<endl;
}


string completarCad(string cadena,int cifras) {
	int longitud=cadena.length();
	stringstream aux;
	aux<<cadena;
	if (longitud<cifras) {
		for (int i=0;i<cifras-longitud+1;i++) {
			aux<<" ";
		}
	}
	return aux.str();
}
void error() {
	cout<<"Error al Procesar"<<endl;
}
void mostrarTile(string tile) {
	system("cls");
	cout<<tile<<endl;
	linea();
}

void mostrarTilev2(string tile) {
	cout<<tile<<endl;
	linea();
}

void mostrarOp(int pos,string opcion) {
	cout<<pos<<". "<<opcion<<endl;
}
void introducir(int &opcion,int max) {
	opcion=0;
	cout<<"Introducir Opcion: ";cin>>opcion; linea();
	if (opcion>max || opcion<=0) {
		introducir(opcion,max);
	}
}

//CONVERSIONES--------------------------------------------------------
//A STRING
string INTtoSTRING(int num) {
	stringstream cad;
	cad<<num;
	return cad.str();
}
string FLOATtoSTRING(float num) {
	stringstream cad;
	cad<<num;
	return cad.str();
}
string DOUBLEtoSTRING(double num) {
	stringstream cad;
	cad<<num;
	return cad.str();
}
string LONGtoSTRING(long num) {
	stringstream cad;
	cad<<num;
	return cad.str();
}
string CHARtoSTRING(char caract) {
	stringstream cad_aux;
	cad_aux<<caract;
	return cad_aux.str();
}
string CHARtoSTRINGarray(char caract[],int cant) {
	stringstream cad_aux;
	for (int i=0;i<cant;i++) {
		cad_aux<<caract[i];
	}
	return cad_aux.str();
}
//DE STRING
int STRINGtoINT(string cad) {
	stringstream cad_aux;
	int num;
	cad_aux<<cad;
	cad_aux>>num;
	return num;
}
float STRINGtoFLOAT(string cad) {
	stringstream cad_aux;
	float num;
	cad_aux<<cad;
	cad_aux>>num;
	return num;
}
double STRINGtoDOUBLE(string cad) {
	stringstream cad_aux;
	double num;
	cad_aux<<cad;
	cad_aux>>num;
	return num;
}
long STRINGtoLONG(string cad) {
	stringstream cad_aux;
	long num;
	cad_aux<<cad;
	cad_aux>>num;
	return num;
}
char STRINGtoCHAR(string cad) {	//DE 1 CARACTER
	char char_aux;
	stringstream cad_aux;
	cad_aux<<cad;
	cad_aux>>char_aux;
	return char_aux;
}
void STRINGtoCHARarray(string cad, char char_aux[]) {
	int longitud=cad.length();
	for (int i=0;i<longitud;i++) {
		char_aux[i]=STRINGtoCHAR(cad.substr(i,1));
	}
}

//AUXILIARES----------------------------------------------------------
void limpiarCHARarray(char caract[],int tamanio) {
	for (int i=0;i<tamanio;i++) {
		caract[i]=' ';
	}
}
void limpiarStringArray(string cadena[],int cant) {
	for (int i=0;i<cant;i++) {
		cadena[i]=" ";
	}
}
