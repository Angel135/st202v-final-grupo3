#ifndef UTILITARIOS_UTILITARIOS_HPP_
#define UTILITARIOS_UTILITARIOS_HPP_
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <map>
#include <vector>
using namespace std;
//ARCHIVOS TXT--------------------------------------------------------
const string datosCelulares="datos/celulares.txt";
const string datosUsuarios="datos/usuarios.txt";
const string datosVendedor="datos/vendedor.txt";
const string datosAdmin="datos/admin.txt";
const string datosFiltros="datos/filtros.txt";
const string datosCarritoTemporal="datos/carrito.txt";
const string datosHistorial="datos/historial.txt";
//--------------------------------------------------------------------
void linea();
void lineaCorta();
string completarCad(string cadena,int cifras);
void error();
void mostrarTile(string tile);
void mostrarTilev2(string tile);
void mostrarOp(int pos,string opcion);
void introducir(int &opcion,int max);
//CONVERSIONES--------------------------------------------------------
//A STRING
string INTtoSTRING(int num);
string FLOATtoSTRING(float num);
string DOUBLEtoSTRING(double num);
string LONGtoSTRING(long num);
string CHARtoSTRING(char caract);
string CHARtoSTRINGarray(char caract[],int cant);
//DE STRING
int STRINGtoINT(string cad);
float STRINGtoFLOAT(string cad);
double STRINGtoDOUBLE(string cad);
long STRINGtoLONG(string cad);
char STRINGtoCHAR(string cad);
void STRINGtoCHARarray(string cad, char char_aux[]);
//--------------------------------------------------------------------
void limpiarCHARarray(char caract[],int tamanio);
void limpiarStringArray(string cadena[],int cant);
//--------------------------------------------------------------------
struct camara {
	int camFront2;
	int camRear2;
	int camFrontSize;
	int camRearSize;
	string generarCadena() {
		string cad;
		cad =INTtoSTRING(camFront2)+",";
		cad+=INTtoSTRING(camRear2)+",";
		cad+=INTtoSTRING(camFrontSize)+",";
		cad+=INTtoSTRING(camRearSize)+",";
		return cad;
	}
};
struct pantalla {
	string screenResol;
	float screenSize;
	string screenTech;
	string generarCadena() {
		string cad;
		cad =screenResol+",";
		cad+=FLOATtoSTRING(screenSize)+",";
		cad+=screenTech+",";
		return cad;
	}
};
struct apariencia {
	string color;
	string material;
	int notch;
	string resistencia;
	int proteccion;
	string generarCadena() {
		string cad;
		cad =color+",";
		cad+=material+",";
		cad+=INTtoSTRING(notch)+",";
		cad+=resistencia+",";
		cad+=INTtoSTRING(proteccion)+",";
		return cad;
	}
};
struct seguridad {
	int huella;
	string huellaPos;
	int faceUnlock;
	string generarCadena() {
		string cad;
		cad =INTtoSTRING(huella)+",";
		cad+=huellaPos+",";
		cad+=INTtoSTRING(faceUnlock)+",";
		return cad;
	}
};
struct bateria {
	int bateria;	//EXTRAIBLE O NO
	int capacidad;
	int cargaInalamb;
	string generarCadena() {
		string cad;
		cad =INTtoSTRING(bateria)+",";
		cad+=INTtoSTRING(capacidad)+",";
		cad+=INTtoSTRING(cargaInalamb)+",";
		return cad;
	}
};
struct compInterno {
	string procesador;
	int ram;
	int rom;
	string generarCadena() {
		string cad;
		cad =procesador+",";
		cad+=INTtoSTRING(ram)+",";
		cad+=INTtoSTRING(rom)+",";
		return cad;
	}
};
struct extras {
	int sd;
	int hpJack;
	string entrada;
	int nfc;
	int radio;
	int infrarrojo;
	string generarCadena() {
		string cad;
		cad =INTtoSTRING(sd)+",";
		cad+=INTtoSTRING(hpJack)+",";
		cad+=entrada+",";
		cad+=INTtoSTRING(nfc)+",";
		cad+=INTtoSTRING(radio)+",";
		cad+=INTtoSTRING(infrarrojo)+",";
		return cad;
	}
};
struct sistema {
	string so;
	string generarCadena() {
		string cad;
		cad =so+",";
		return cad;
	}
};
struct informacion {
	string marca;
	string modelo;
	//string nombre=marca+" "+modelo;
	string descripcion;
	float precio;
	int stock;
	string generarCadena() {
		string cad;
		cad =marca+",";
		cad+=modelo+",";
		cad+=marca+" "+modelo+",";
		cad+=descripcion+",";
		cad+=FLOATtoSTRING(precio)+",";
		cad+=INTtoSTRING(stock)+",";
		return cad;
	}
};
struct conectividad {
	int dobleSim;
	int freq4g;
	string generarCadena() {
		string cad;
		cad =INTtoSTRING(dobleSim)+",";
		cad+=INTtoSTRING(freq4g)+",";
		return cad;
	}
};
struct celular {
	struct camara camaraY;
	struct pantalla pantallaY;
	struct apariencia aparienciaY;
	struct seguridad seguridadY;
	struct bateria bateriaY;
	struct compInterno compInternoY;
	struct extras extrasY;
	struct sistema sistemaY;
	struct informacion informacionY;
	struct conectividad conectividadY;
	string generarCadena() {
		string cad;
		cad =informacionY.generarCadena();
		cad+=camaraY.generarCadena();
		cad+=pantallaY.generarCadena();
		cad+=aparienciaY.generarCadena();
		cad+=seguridadY.generarCadena();
		cad+=bateriaY.generarCadena();
		cad+=compInternoY.generarCadena();
		cad+=extrasY.generarCadena();
		cad+=sistemaY.generarCadena();
		cad+=conectividadY.generarCadena();
		return cad;
	}
};
#endif /* UTILITARIOS_UTILITARIOS_HPP_ */
