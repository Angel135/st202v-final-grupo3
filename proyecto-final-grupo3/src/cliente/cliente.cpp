/*
 * cliente.cpp
 *
 *  Created on: 2 jun. 2018
 *      Author: Riuzer
 */

#include <iostream>
#include <cstring>
#include "../lobby/lobby.hpp"
#include "../cliente/cliente.hpp"
#include "../buscar/buscar.hpp"
#include "../buscar_v2/buscar_v2.hpp"
#include "../buscar_v3/buscar_v3.hpp"
#include "../login/login.hpp"
#include "../utilitarios/utilitarios.hpp"
#include "../vendedor/vendedor.hpp"
#include "../admin/admin.hpp"
using namespace std;

void registroCliente();
void buscarRecomendados_sin_reg();
struct cliente {
	string nombre_completo;
	int DNI;
	string nombre_usuario;
	string contrasenia;
	int verificar_contrasenia;
	string e_mail;
	int telefono;
};
struct cliente nuevo1;

void recomendados(){
	int recomendar=0;
	mostrarTile("Desea ver recomendaciones?");
	mostrarOp(1,"Si");
	mostrarOp(2,"No");
	mostrarOp(3,"Atras");
	cin>>recomendar;

	switch(recomendar){
		case 1:
				cin.ignore();
				mostrarTilev2("*******Los recomendados del dia de hoy son*******");

				break;
		case 2:
				cin.ignore();
				mostrarTile("Gracias por su visita");
				break;
		case 3: menucliente();
			break;
	};
}

void comparar(){
	string codigo1, codigo2;
	int compar=0;
	int regresar=0;

	ifstream celulares;
		celulares.open("datos/celulares.txt");
		if(celulares.is_open()){
		while(!celulares.eof()){
			string linea;
			//getline(celulares,',');
			cout<<linea<<endl;
			}
			}else{
			cout<<"error al abrir el archivo"<<endl;
			}



	mostrarTile("?Desea comparar 2 modelos de celular?");
	mostrarOp(1,"Si");
	mostrarOp(2,"No");
	mostrarOp(3,"Atras");
	cin>>compar;
	switch(compar){
			case 1:
				cin.ignore();
				mostrarTile("Digite el codigo del primer celular");
				getline(cin,codigo1);
				mostrarTile("Digite el codigo del segundo celular");
				getline(cin,codigo2);
				system("cls");

			mostrarTilev2("Porcesando datos... .... ...");
			mostrarTilev2("******Empezando la comparacion******");
			void linea();
			mostrarTilev2("******APARIENCIA******");

			mostrarTilev2("Tema =====>  Apariencia color ");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Apariencia Material");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Apariencia Notch");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Apariencia Proteccion");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Apariencia Resistencia ");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();
			mostrarTilev2("******BATERIA******");

			mostrarTilev2("Tema =====>  Bateria Capacidad");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Bateria Extraible");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Bateria Carga Inalambrica");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******CAMARAS******");

			mostrarTilev2("Tema =====>  Camara Principal");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Camara Principal Dual");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Camara Frontal");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====> Camara Frontal Dual");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******COMPONENTES******");

			mostrarTilev2("Tema =====>  Componente Procesador");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Componente RAM");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Componente ROM");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******CONECTIVIDAD******");

			mostrarTilev2("Tema =====>  Conectividad Doble SIM");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Conectividad 4G");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******EXTRAS******");

			mostrarTilev2("Tema =====>  Extras SD");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Extras Jack de Audifonos");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Extras Tipo de Entrada");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Extras NFC");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Extras Radio");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Extras Infrarrojo");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******INFORMACION******");

			mostrarTilev2("Tema =====>  Informacion Marca");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Informacion Modelo");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Informacion Precio");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Informacion Stock");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******PANTALLA******");

			mostrarTilev2("Tema =====>  Pantalla Resolucion");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Pantalla Tamanio");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Pantalla Tecnologia");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******SEGURIDAD******");

			mostrarTilev2("Tema =====>  Seguridad Face Unlock");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Seguridad Huella");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Seguridad Huella Posicion");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******SISTEMA OPERATIVO******");

			mostrarTilev2("Tema =====>  Software SO");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			void linea();

			mostrarTilev2("******FIN DE LA COMPARACION******");

			/*
			void linea();
			mostrarTilev2("Tema =====>  Maca");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Modelo");
			mostrarTilev2("Celular 1:                 Celular 2:");

			void linea();
			mostrarTilev2("Tema =====>  Precio");
			mostrarTilev2("Celular 1:                 Celular 2:");
*/

			mostrarTilev2("Regresar al menucliente");
			mostrarOp(1,"Si");
			mostrarOp(2,"No");
			cin>>regresar;
			switch(regresar){
				case 1:	menucliente();
					break;
				case 2: mostrarTile("Gracias por su visita");
					break;
			}

		break;

		case 2: mostrarTile("Gracias por su visita");
			break;
		case 3: menucliente();
			break;
		};
}


void comprar(){
	int comprarr=0;
	char code[20];
	int confirmar=0,confirmar2=0,acumular=0;

	mostrarTile("?Desea comprar");
	mostrarOp(1,"Si");
	mostrarOp(2,"No");
	mostrarOp(3,"Atras");
	cin>>comprarr;
	switch(comprarr){
		case 1:
			mostrarTile("Escriba el codigo del producto que desea que desea comprar");
			cin>>code[0];
			cin.ignore();
			mostrarTile("?Quieres agregar otro producto?");
			mostrarOp(1,"Si");
			mostrarOp(2,"No");
			cin>>confirmar;
			cin.ignore();
			while(confirmar==1){
				for(int i=1;i<100;i++){
					mostrarTile("Escriba el codigo del producto que desea que desea agregar");
					cin>>code[i];
				}

			}


/*
			if(confirmar==1){
				for(int i=1;i<100;i++){
					mostrarTile("Escriba el codigo del producto que desea que desea agregar");
					cin>>code[i];
					mostrarTile("?Quieres agregar otro producto?");
					mostrarOp(1,"Si");
					mostrarOp(2,"No");
					cin>>confirmar2;
					cin.ignore();
					while(confirmar==1){
						mostrarTile("Escriba el codigo del producto que desea que desea agregar");
						cin>>code[i];
					};
					acumular=acumular+1;
				}

			}
			else if(confirmar==0){
				cout<<"Fin de la compra"<<endl;
			}
				//Se iran imprimiendo los productos a comprar

				cout<<"Total de productos a comprar "<<acumular+1<<"."<<endl;
*/
			break;
		case 2: mostrarTile("Gracias por su visita");
			break;
		case 3: menucliente();
			break;
		};
}
/*
void modificar_datos_clientes(){
	int modificar=0, opcion=0;
	mostrarTile("?Desea modificar sus datos?");
	mostrarOp(1,"Si");
	mostrarOp(2,"No");
	mostrarOp(3,"Atras");
	cin>>modificar;

	ifstream usuarios;
			usuarios.open("datos/celulares.txt");
			if(usuarios.is_open()){
			while(!usuarios.eof()){
				string linea;
				//getline(usuarios,",");
				cout<<linea<<endl;
				}
				}else{
				cout<<"error al abrir el archivo"<<endl;
				}


	switch(modificar){
	case 1:
		do{
			mostrarTile("Que campo desea modificar");
			mostrarOp(1,"Nombre completo");
			mostrarOp(2,"DNI");
			mostrarOp(3,"Nombre de usuario");
			mostrarOp(4,"Contrasenia");
			mostrarOp(5,"e-mail");
			mostrarOp(6,"Numero de telefono");
			cin>>opcion;

			switch (opcion){
				case 1:

					break;
				case 2:

					break;
				case 3:

					break;
				case 4:

					break;
				case 5:

					break;
				case 6:

					break;
				default: mostrarTile("Opcion  incorrecta");
				modificar_datos_clientes();
			}
		}while(opcion<7);
		mostrarTile("Regresando al lobby");
		lobby();
		break;
	case 2:
		mostrarTile("Los datos se mantendran.");
		mostrarTile("Regresando al lobby");
		lobby();
	break;
	case 3: menucliente();
	}
	system("cls");
}
*/

void menucliente_sin_registrar(){
	int opcioncliente=0,op=0,op1=0;
	mostrarTile("Seleccione una opcion");
	mostrarOp(1,"Smartphones Recomendados");
	mostrarOp(2,"Buscar y Comparar");
	mostrarOp(3,"Comprar");
	mostrarOp(4,"Modificar datos del cliente");
	mostrarOp(5,"Atras");
	cin>>opcioncliente;
	switch(opcioncliente){
	case 1:
		//buscarRecomendados_sin_reg();
		break;
	case 2:

		mostrarTile("Primero necesita registrarse");
		mostrarTile("?Desea registrarse?");
		mostrarOp(1,"Si");
		mostrarOp(2,"No");
		cin>>op;
		switch(op){
			case 1:
				registroCliente();
				break;
			case 2:
				mostrarTile("Redireccionando al inicio.");
				menuPrincipal();
		}
		break;
	case 3:
	mostrarTile("Primero necesita registrarse");
	mostrarTile("?Desea registrarse?");
	mostrarOp(1,"Si");
	mostrarOp(2,"No");
	cin>>op1;
	switch(op1){
		case 1:
			registroCliente();
			break;
		case 2:
			mostrarTile("Redireccionando al inicio.");
			menuPrincipal();
	}
		break;
	case 4: mostrarTile("Primero necesita registrarse");
		registroCliente();
		break;
	case 5:
		menuPrincipal();
		break;
	}
}

void menucliente(){
	int opcioncliente=0;
	mostrarTile("Seleccione una opcion");
	mostrarOp(1,"Ver recomendados");
	mostrarOp(2,"Comparar");
	mostrarOp(3,"Comprar");
	mostrarOp(4,"Modificar datos del cliente");
	mostrarOp(5,"Atras");
	cin>>opcioncliente;
	switch(opcioncliente){
	case 1:	recomendados();
		break;
	case 2: comparar();
		break;
	case 3: comprar();
		break;
	case 4:
		break;
	case 5: //lobby();
		break;
	}
}




