#include "../utilitarios/utilitarios.hpp"

//VARIABLES----------------------------------------------------------

int opcion;
int opcion2;
int w,x,y,z;
int contador1,contador2;
string lineas[100];
int camFront2X;
int camRear2X;
int camFrontSizeX;
int camRearSizeX;
string screenResolX;
float screenSizeX;
string screenTechX;
string colorX;
string materialX;
int pesoX;
int notchX;
string resistenciaX;
int proteccionX;
int huellaX;
string huellaPosX;
int faceUnlockX;
int bateriaX;
int capacidadX;
int cargaInalambX;
string procesadorX;
int ramX;
int romX;
int sdX;
int hpJackX;
string entradaX;
int nfcX;
int radioX;
int infrarrojoX;
string soX;
string osVerX;
int dobleSimX;
int freq4gX;
string marcaX;
string modeloX;
string nombreX;
float precioX;
int stockX;

void limpiar() {
	opcion=0;
	camFront2X=0;
	camRear2X=0;
	camFrontSizeX=0;
	camRearSizeX=0;
	screenResolX="0";
	screenSizeX=0.0;
	screenTechX="0";
	colorX="0";
	materialX="0";
	pesoX=0;
	notchX=0;
	resistenciaX="0";
	proteccionX=0;
	huellaX=0;
	huellaPosX="0";
	faceUnlockX=0;
	bateriaX=0;
	capacidadX=0;
	cargaInalambX=0;
	procesadorX="0";
	ramX=0;
	romX=0;
	sdX=0;
	hpJackX=0;
	entradaX="0";
	nfcX=0;
	radioX=0;
	infrarrojoX=0;
	soX="0";
	osVerX="0";
	dobleSimX=0;
	freq4gX=0;
	marcaX="0";
	modeloX="0";
	nombreX=marcaX+" "+modeloX;
	precioX=0.0;
	stockX=0;
}

//FILTROS------------------------------------------------------------

void buscarBool(string tile,int &filtro) {
	mostrarTile(tile);
	mostrarOp(1,"Si");
	mostrarOp(2,"No");
	mostrarOp(3,"Regresar");linea();
	introducir(opcion,3);
	switch (opcion) {
		case 1: filtro=1;
			break;
		case 2: filtro=2;
			break;
	}
}

void coutBool(string valor) {
	if (valor=="1") {
		cout<<"Si"<<endl;
	}
	else if (valor=="2") {
		cout<<"No"<<endl;
	}
}

string buscarMarca() {
	mostrarTile("MARCA");
	mostrarOp(1,"Samsung");
	mostrarOp(2,"Huawei");
	mostrarOp(3,"Apple");
	mostrarOp(4,"Nokia");
	mostrarOp(5,"Motorola");
	mostrarOp(6,"Oneplus");
	mostrarOp(7,"Google");
	mostrarOp(8,"Lenovo");
	mostrarOp(9,"Regresar"); linea();
	introducir(opcion,9);
	switch (opcion) {
		case 1: return "Samsung";
			break;
		case 2: return "Huawei";
			break;
		case 3: return "Apple";
			break;
		case 4: return "Nokia";
			break;
		case 5: return "Motorola";
			break;
		case 6: return "Oneplus";
			break;
		case 7: return "Google";
			break;
		case 8: return "Lenovo";
			break;
		default:
			return "NA";
	}
}
/*
void buscarPrecio() {	//CORREGIR!
	mostrarTile("PRECIO");
	mostrarOp(1,"<$200");
	mostrarOp(2,"$200-$300");
	mostrarOp(3,"$300-$400");
	mostrarOp(4,"$400-$500");
	mostrarOp(5,"$500>"); linea();
	introducir(opcion,5);
	switch (opcion) {
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
	}
}
*/
string buscarProcesador() {
	mostrarTile("PROCESADOR");
	mostrarOp(1,"Snapdragon");
	mostrarOp(2,"Mediatek");
	mostrarOp(3,"Kirin");
	mostrarOp(4,"Exynos");
	mostrarOp(5,"Apple");
	mostrarOp(6,"Regresar"); linea();
	introducir(opcion,6);
	switch (opcion) {
		case 1: return "Snapdragon";
			break;
		case 2: return "Mediatek";
			break;
		case 3: return "Kirin";
			break;
		case 4: return "Exynos";
			break;
		case 5: return "Apple";
			break;
		default:
			return "NA";
	}
}

int buscarRAM() {
	mostrarTile("RAM");
	mostrarOp(1,"1GB");
	mostrarOp(2,"2GB");
	mostrarOp(3,"3GB");
	mostrarOp(4,"4GB");
	mostrarOp(5,"6GB");
	mostrarOp(6,"Regresar"); linea();
	introducir(opcion,6);
	switch (opcion) {
		case 1: return 1;
			break;
		case 2: return 2;
			break;
		case 3: return 3;
			break;
		case 4: return 4;
			break;
		case 5: return 6;
			break;
		default:
			return 0;
	}
}

int buscarROM() {
	mostrarTile("ROM");
	mostrarOp(1,"8GB");
	mostrarOp(2,"16GB");
	mostrarOp(3,"32GB");
	mostrarOp(4,"64GB");
	mostrarOp(5,"128GB");
	mostrarOp(6,"256GB");
	mostrarOp(7,"Regresar"); linea();
	introducir(opcion,7);
	switch (opcion) {
		case 1: return 8;
			break;
		case 2: return 16;
			break;
		case 3: return 32;
			break;
		case 4: return 64;
			break;
		case 5: return 128;
			break;
		case 6: return 256;
			break;
		default:
			return 0;
	}
}

string buscarColor() {
	mostrarTile("COLOR");
	mostrarOp(1,"Azul");
	mostrarOp(2,"Negro");
	mostrarOp(3,"Gris");
	mostrarOp(4,"Rojo");
	mostrarOp(5,"Rosa");
	mostrarOp(6,"Dorado");
	mostrarOp(7,"Blanco");
	mostrarOp(8,"Regresar"); linea();
	introducir(opcion,8);
	switch (opcion) {
		case 1: return "Azul";
			break;
		case 2: return "Negro";
			break;
		case 3: return "Gris";
			break;
		case 4: return "Rojo";
			break;
		case 5: return "Rosa";
			break;
		case 6: return "Dorado";
			break;
		case 7: return "Blanco";
			break;
		default:
			return "NA";
	}
}

string buscarMaterial() {
	mostrarTile("MATERIAL");
	mostrarOp(1,"Plastico");
	mostrarOp(2,"Aluminio");
	mostrarOp(3,"Vidrio");
	mostrarOp(4,"Zafiro");
	mostrarOp(5,"Regresar"); linea();
	introducir(opcion,5);
	switch (opcion) {
		case 1: return "Plastico";
			break;
		case 2: return "Aluminio";
			break;
		case 3: return "Vidrio";
			break;
		case 4: return "Zafiro";
			break;
		default:
			return "NA";
	}
}

string buscarResistencia() {
	mostrarTile("Resistencia al agua");
	mostrarOp(1,"A la sumersion");
	mostrarOp(2,"A salpicaduras");
	mostrarOp(3,"No es resistente");
	mostrarOp(4,"Regresar"); linea();
	introducir(opcion,4);
	switch (opcion) {
		case 1: return "Sumersion";
			break;
		case 2: return "Salpicadura";
			break;
		case 3: return "No";
			break;
	}
	return "NA";
}

int buscarProteccion() {
	mostrarTile("Proteccion");
	mostrarOp(1,"Gorilla Glass 3");
	mostrarOp(2,"Gorilla Glass 4");
	mostrarOp(3,"Gorilla Glass 5");
	mostrarOp(4,"Regresar"); linea();
	introducir(opcion,4);
	switch (opcion) {
		case 1: return 3;
			break;
		case 2: return 4;
			break;
		case 3: return 5;
			break;
		default:
			return 0;
	}
}

string buscarResolucion() {
	mostrarTile("Resolucion");
	mostrarOp(1,"HD");
	mostrarOp(2,"FHD");
	mostrarOp(3,"QHD");
	mostrarOp(4,"Regresar"); linea();
	introducir(opcion,4);
	switch (opcion) {
		case 1: return "HD";
			break;
		case 2: return "FHD";
			break;
		case 3: return "QHD";
			break;
		default:
			return "NA";
	}
}

float buscarTamanio() {
	mostrarTile("Tamanio");
	mostrarOp(1,"4.5'");
	mostrarOp(2,"4.7'");
	mostrarOp(3,"5'");
	mostrarOp(4,"5.5'");
	mostrarOp(5,"6'");
	mostrarOp(6,"Regresar"); linea();
	introducir(opcion,6);
	switch (opcion) {
		case 1: return (4.5);
			break;
		case 2:return (4.7);
			break;
		case 3: return (5);
			break;
		case 4: return (5.5);
			break;
		case 5: return 6;
			break;
		default:
			return 0;
	}
}

string buscarTecnologia() {
	mostrarTile("Tamanio");
	mostrarOp(1,"IPS");
	mostrarOp(2,"OLED");
	mostrarOp(3,"Regresar"); linea();
	introducir(opcion,3);
	switch (opcion) {
		case 1: return "IPS";
			break;
		case 2: return "OLED";
			break;
		default:
			return "NA";
	}
}
/*
string buscarBateria() {	//CORREGIR!
	mostrarTile("Bateria");
	mostrarOp(1,"<2500 mAh");
	mostrarOp(2,"2500-3000 mAh");
	mostrarOp(3,">3000 mAh");
	mostrarOp(4,"Regresar");  linea();
	introducir(opcion,4);
	switch (opcion) {
		case 1:
			break;
		case 2:
			break;
		case 3:
			break;
		default:
			return "NA";
	}
}
*/
string buscarSO() {
	mostrarTile("Sistema Operativo");
	mostrarOp(1,"Android");
	mostrarOp(2,"iOS");
	mostrarOp(3,"WP");
	mostrarOp(4,"Regresar"); linea();
	introducir(opcion,4);
	switch (opcion) {
		case 1: return "Android";
			break;
		case 2: return "iOS";
			break;
		case 3: return "WP";
			break;
		default:
			return "NA";
	}
}

int buscarMpx() {
	mostrarTile("MEGAPIXELES");
	mostrarOp(1,"5MP");
	mostrarOp(2,"8MP");
	mostrarOp(3,"12MP");
	mostrarOp(4,"16MP");
	mostrarOp(5,"20MP");
	mostrarOp(6,"Regresar"); linea();
	introducir(opcion,6);
	switch (opcion) {
		case 1: return 5;
			break;
		case 2: return 8;
			break;
		case 3: return 12;
			break;
		case 4: return 16;
			break;
		case 5: return 20;
			break;
		default:
			return 0;
	}
}

void buscarCamRear() {
	mostrarTile("CAMARA PRINCIPAL");
	mostrarOp(1,"Megapixeles");
	mostrarOp(2,"Dual");
	mostrarOp(3,"Regresar");linea();
	introducir(opcion,3);
	switch (opcion) {
		case 1: camRearSizeX=buscarMpx();
				buscarCamRear();
			break;
		case 2: buscarBool("CAMARA DUAL",camRear2X);
				buscarCamRear();
			break;
	}
}

void buscarCamFront() {
	mostrarTile("CAMARA FRONTAL");
	mostrarOp(1,"Megapixeles");
	mostrarOp(2,"Dual");
	mostrarOp(3,"Regresar"); linea();
	introducir(opcion,3);
	switch (opcion) {
		case 1: camFrontSizeX=buscarMpx();
				buscarCamFront();
			break;
		case 2: buscarBool("CAMARA DUAL",camFront2X);
				buscarCamFront();
			break;
	}
}

void buscarCamara() {
	mostrarTile("CAMARA");
	mostrarOp(1,"Principal");
	mostrarOp(2,"Frontal");
	mostrarOp(3,"Regresar");linea();
	introducir(opcion,3);
	switch (opcion) {
		case 1: buscarCamRear();
				buscarCamara();
			break;
		case 2: buscarCamFront();
				buscarCamara();
			break;
	}
}

void buscarHuella() {
	buscarBool("SENSOR DE HUELLA",huellaX);
	if (huellaX==true) {
		mostrarTile("POSICION");
		mostrarOp(1,"Frontal");
		mostrarOp(2,"Trasera");
		mostrarOp(3,"Regresar"); linea();
		introducir(opcion,3);
		switch (opcion) {
			case 1: huellaPosX="Frontal";
				break;
			case 2: huellaPosX="Trasera";
				break;
		}
	}
}

string buscarEntrada() {
	mostrarTile("PUERTO DE ENTRADA");
	mostrarOp(1,"Micro USB");
	mostrarOp(2,"USB tipo C");
	mostrarOp(3,"Lightning"); linea();
	introducir(opcion,3);
	switch (opcion) {
		case 1: return "MicroUSB";
			break;
		case 2: return "TipoC";
			break;
		case 3: return "Lightning";
			break;
		default:
			return "NA";
	}
}

//BUSQUEDA POR FILTROS-----------------------------------------

void filtros() {

	limpiar();
	int opcion;
	mostrarTile("BUSCAR POR FILTROS");
	mostrarOp(1,"Informacion");
	mostrarOp(2,"Componentes");
	mostrarOp(3,"Apariencia");
	mostrarOp(4,"Pantalla");
	mostrarOp(5,"Bateria");
	mostrarOp(6,"Sistema Operativo");
	mostrarOp(7,"Conectividad");
	mostrarOp(8,"Camara");
	mostrarOp(9,"Seguridad");
	mostrarOp(10,"Extras");
	mostrarOp(11,"BUSCAR!");
	mostrarOp(12,"Salir");	linea();
	introducir(opcion,12);
	switch (opcion) {
		case 1: mostrarTile("INFORMACION");
				mostrarOp(1,"Marca");
				mostrarOp(2,"Precio");
				mostrarOp(3,"Regresar");linea();
				introducir(opcion,3);
				switch (opcion) {
					case 1: marcaX=buscarMarca();
						break;
					case 2: //buscarPrecio();
						break;
				}
				filtros();
			break;
		case 2: mostrarTile("COMPONENTES");
				mostrarOp(1,"Procesador");
				mostrarOp(2,"RAM");
				mostrarOp(3,"ROM");
				mostrarOp(4,"Regresar"); linea();
				introducir(opcion,4);
				switch (opcion) {
					case 1: procesadorX=buscarProcesador();
						break;
					case 2: ramX=buscarRAM();
						break;
					case 3: romX=buscarROM();
						break;
				}
				filtros();
			break;
		case 3: mostrarTile("APARIENCIA");
				mostrarOp(1,"Color");
				mostrarOp(2,"Material");
				mostrarOp(3,"Notch");
				mostrarOp(4,"Resistencia");
				mostrarOp(5,"Proteccion");
				mostrarOp(6,"Regresar"); linea();
				introducir(opcion,6);
				switch (opcion) {
					case 1: colorX=buscarColor();
						break;
					case 2: materialX=buscarMaterial();
						break;
					case 3: buscarBool("NOTCH",notchX);
						break;
					case 4: resistenciaX=buscarResistencia();
						break;
					case 5: proteccionX=buscarProteccion();
						break;
				}
				filtros();
			break;
		case 4: mostrarTile("PANTALLA");
				mostrarOp(1,"Resolucion");
				mostrarOp(2,"Tamanio");
				mostrarOp(3,"Tecnologia");
				mostrarOp(4,"Regresar");linea();
				introducir(opcion,4);
				switch (opcion) {
					case 1: screenResolX=buscarResolucion();
						break;
					case 2: screenSizeX=buscarTamanio();
						break;
					case 3: screenTechX=buscarTecnologia();
						break;
				}
				filtros();
			break;
		case 5: mostrarTile("BATERIA");
				mostrarOp(1,"Bateria Extraible");
				mostrarOp(2,"Capacidad");
				mostrarOp(3,"Carga inalambrica");
				mostrarOp(4,"Regresar"); linea();
				introducir(opcion,4);
				switch (opcion) {
					case 1: buscarBool("BATERIA REMOVIBLE",bateriaX);
						break;
					case 2: //buscarBateria();
						break;
					case 3: buscarBool("CARGA INALAMBRICA",cargaInalambX);
						break;
				}
				filtros();
			break;
		case 6: soX=buscarSO();
				filtros();
			break;
		case 7: mostrarTile("CONECTIVIDAD");
				mostrarOp(1,"Doble SIM");
				mostrarOp(2,"Compatible 4G");
				mostrarOp(3,"Regresar");linea();
				introducir(opcion,3);
				switch (opcion) {
					case 1: buscarBool("DOBLE SIM",dobleSimX);
						break;
					case 2: buscarBool("4G",freq4gX);
						break;
				}
				filtros();
			break;
		case 8: buscarCamara();
				filtros();
			break;
		case 9: mostrarTile("SEGURIDAD");
				mostrarOp(1,"Sensor de huella");
				mostrarOp(2,"Face unlock");
				mostrarOp(3,"Regresar"); linea();
				introducir(opcion,2);
				switch (opcion) {
					case 1: buscarHuella();
						break;
					case 2: buscarBool("Face Unlock",faceUnlockX);
						break;
				}
				filtros();
			break;
		case 10: mostrarTile("EXTRAS");
				mostrarOp(1,"Expansion microSD");
				mostrarOp(2,"Jack de audifonos");
				mostrarOp(3,"Puerto de entrada");
				mostrarOp(4,"NFC");
				mostrarOp(5,"Radio");
				mostrarOp(6,"Infrarrojo"); linea();
				introducir(opcion,6);
				switch (opcion) {
					case 1: buscarBool("EXPANSON MICROSD",sdX);
						break;
					case 2: buscarBool("JACK DE AUDIFIONOS",hpJackX);
						break;
					case 3: entradaX=buscarEntrada();
						break;
					case 4: buscarBool("NFC",nfcX);
						break;
					case 5: buscarBool("RADIO",radioX);
						break;
					case 6: buscarBool("INFRARROJO",infrarrojoX);
						break;
				}
				filtros();
			break;
		case 11:
			break;
		case 12: system("exit");
			break;
			//funcion menu principal
	}
}

//VERIFICAR SI COINCIDE----------------------------------------------

void verificarCadena(string cadena1,string cadena2) {
	if (cadena1!="0") {
		if (cadena1==cadena2) {
			x++;
		}
	}
	else {
		y++;
	}
}

void verificarEntero(int numero,string cadena1) {
	if (numero!=0) {
		if (numero==STRINGtoINT(cadena1)) {
			x++;
		}
	}
	else {
		y++;
	}
}

void verificarFloat(float numero,string cadena1) {
	if (numero!=0.0) {
		if (numero==STRINGtoFLOAT(cadena1)) {
			x++;
		}
	}
	else {
		y++;
	}
}

//MOSTRAR RESULTADOS-------------------------------------------------

void mostrarBoolValor(string cadena) {
	cout<<"  "<<cadena<<": ";
	if (cadena=="1") {
		cout<<"Si"<<endl;
	}
	else if (cadena=="2") {
		cout<<"No"<<endl;
	}
}

void mostrarPrecioStock(int pos) {
	stringstream aux;
	string lineaaux;
	aux<<lineas[pos-1];
	getline(aux,lineaaux,',');
	getline(aux,lineaaux,',');
	getline(aux,lineaaux,',');
	int longitud=lineaaux.length();
	cout<<lineaaux;
	for (int i=0;i<22-longitud+1;i++) {
		cout<<" ";
	}
	getline(aux,lineaaux,',');
	getline(aux,lineaaux,',');
	longitud=lineaaux.length();
	cout<<lineaaux<<endl;
	for (int i=0;i<11-longitud+1;i++) {
		cout<<" ";
	}
	getline(aux,lineaaux,',');
	cout<<lineaaux<<endl;
}

void mostrarDetalles() {
	stringstream aux;
	int contador=0;
	string lineaaux;
	aux<<lineas[opcion2-1];
	mostrarTile("ESPECIFICACIONES");
	while (getline(aux,lineaaux,',')) {
		contador++;
		switch (contador) {

			case 1: cout<<"Informacion"<<endl;
					cout<<"   Marca: "<<lineaaux<<endl;
				break;
			case 2: cout<<"   Modelo: "<<lineaaux<<endl;
				break;
			case 3: //cout<<"   Nombre: "<<lineaaux<<endl;
				break;
			case 4: cout<<"   Precio: "<<lineaaux<<endl;
				break;

			case 5: cout<<"Camara"<<endl;
					cout<<"  Cam frontal dual: "; coutBool(lineaaux);
				break;
			case 6: cout<<"  Cam principal dual: "; coutBool(lineaaux);
				break;
			case 7: cout<<"  MP frontal: "<<lineaaux<<endl;
				break;
			case 8: cout<<"  MP Principal: "<<lineaaux<<endl;
				break;

			case 9: cout<<"Pantalla"<<endl;
					cout<<"   Resolucion: "<<lineaaux<<endl;
				break;
			case 10: cout<<"   Tamanio: "<<lineaaux<<endl;
				break;
			case 11: cout<<"   Tecnologia: "<<lineaaux<<endl;
				break;
			case 12: cout<<"Apariencia"<<endl;
					 cout<<"   Color: "<<lineaaux<<endl;
				break;
			case 13: cout<<"   Material: "<<lineaaux<<endl;
				break;
			case 14: cout<<"   Notch: "; coutBool(lineaaux);
				break;
			case 15: cout<<"   Resistencia: "<<lineaaux<<endl;
				break;
			case 16: cout<<"   Proteccion: "<<lineaaux<<endl;
				break;
			case 17: cout<<"Seguridad"<<endl;
					 cout<<"   Sensor de huella: ", coutBool(lineaaux);
				break;
			case 18: cout<<"   Posicion del sensor: "<<lineaaux<<endl;
				break;
			case 19: cout<<"   Face Unlock: ", coutBool(lineaaux);
				break;
			case 20: cout<<"Bateria"<<endl;
					 cout<<"   Extraible: ", coutBool(lineaaux);
				break;
			case 21: cout<<"   Capacidad: "<<lineaaux<<endl;
				break;
			case 22: cout<<"  Carga inalambrica: ", coutBool(lineaaux);
				break;
			case 23: cout<<"Mainboard"<<endl;
					 cout<<"   Procesador: "<<lineaaux<<endl;
				break;
			case 24: cout<<"   RAM: "<<lineaaux<<endl;
				break;
			case 25: cout<<"   ROM: "<<lineaaux<<endl;
				break;
			case 26: cout<<"Extras"<<endl;
					 cout<<"  Expansion microSD: ", coutBool(lineaaux);
				break;
			case 27: cout<<"  Jack de audifonos: ", coutBool(lineaaux);
				break;
			case 28: cout<<"   Tipo de entrada: "<<lineaaux<<endl;
				break;
			case 29: cout<<"  NFC: ", coutBool(lineaaux);
				break;
			case 30: cout<<"  Radio: ", coutBool(lineaaux);
				break;
			case 31: cout<<"  Infrarrojo: ", coutBool(lineaaux);
				break;
			case 32: cout<<"Sistema Operativo: "<<endl;
					 cout<<"  SO: "<<lineaaux<<endl;
				break;
			case 33: cout<<"Conectividad"<<endl;
					 cout<<"  Doble SIM: ", coutBool(lineaaux);
				break;
			case 34: cout<<"  4G: ", coutBool(lineaaux);
				break;
		}
	}
	cout<<"Regresar a Resultados"<<endl;
	system("pause");
}

void mostrarResultados() {
	mostrarTile("RESULTADOS");
	cout<<"Se ha encontrado: "<<z<<endl; linea();
	system("pause");
	if (z!=0) {
		for (int i=0;i<40;i++) {
			cout<<" ";
		}
		cout<<"NOMBRE                PRECIO     STOCK"<<endl;
		for (int i=0;i<z;i++) {
			mostrarPrecioStock(i);
		}
		mostrarOp(z+1,"Regresar Menu Filtros");
		introducir(opcion2,z+1);
		if (opcion2==z+1) {
			filtros();
		}
		else {
			mostrarDetalles();
			mostrarResultados();
		}
	}
	else {
		mostrarTile("Volviendo al Menu Principal...");
		system("pause");
	}
}

//BUSQUEDA-----------------------------------------------------------

void buscar() {
	filtros();
	ifstream archivo;
	string linea1,linea2;
	stringstream aux1,aux2;
	archivo.open(datosCelulares.c_str());
	if (archivo.is_open()) {
		while (getline(archivo,linea1,'\n')) {
			aux1<<linea1; z=0,x=0,y=0;
			while (getline(aux2,linea2,',')) {
				z++;
				switch (z) {
					case 1: verificarCadena(marcaX,linea2);
						break;
					case 2: verificarCadena(modeloX,linea2);
						break;
					case 3: //verificarCadena(nombreX,linea2);
						break;
					case 4: verificarFloat(precioX,linea2);
						break;
					case 5: verificarEntero(camFront2X,linea2);
						break;
					case 6: verificarEntero(camRear2X,linea2);
						break;
					case 7: verificarEntero(camFrontSizeX,linea2);
						break;
					case 8: verificarEntero(camRearSizeX,linea2);
						break;
					case 9: verificarCadena(screenResolX,linea2);
						break;
					case 10: verificarFloat(screenSizeX,linea2);
						break;
					case 11: verificarCadena(screenTechX,linea2);
						break;
					case 12: verificarCadena(colorX,linea2);
						break;
					case 13: verificarCadena(materialX,linea2);
						break;
					case 14: verificarEntero(notchX,linea2);
						break;
					case 15: verificarCadena(resistenciaX,linea2);
						break;
					case 16: verificarEntero(proteccionX,linea2);
						break;
					case 17: verificarEntero(huellaX,linea2);
						break;
					case 18: verificarCadena(huellaPosX,linea2);
						break;
					case 19: verificarEntero(faceUnlockX,linea2);
						break;
					case 20: verificarEntero(bateriaX,linea2);
						break;
					case 21: //verificarEntero(capacidadX,linea2);
						break;
					case 22: verificarEntero(cargaInalambX,linea2);
						break;
					case 23: verificarCadena(procesadorX,linea2);
						break;
					case 24: verificarEntero(ramX,linea2);
						break;
					case 25: verificarEntero(romX,linea2);
						break;
					case 26: verificarEntero(sdX,linea2);
						break;
					case 27: verificarEntero(hpJackX,linea2);
						break;
					case 28: verificarCadena(entradaX,linea2);
						break;
					case 29: verificarEntero(nfcX,linea2);
						break;
					case 30: verificarEntero(radioX,linea2);
						break;
					case 31: verificarEntero(infrarrojoX,linea2);
						break;
					case 32: verificarCadena(soX,linea2);
						break;
					case 33: verificarEntero(dobleSimX,linea2);
						break;
					case 34: verificarEntero(freq4gX,linea2);
						break;
				}//SWITCH
			}//WHILE GETLINE LINEA2
			if (x+y==32) {
				lineas[w]=linea1;
				w++;
			}
			cout<<x<<endl;
			cout<<y<<endl;
		}//WHILE GETLINE LINEA1
		mostrarResultados();
	}
	else {
		error();
	}
	//menu principal();
}
