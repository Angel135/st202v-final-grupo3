#include "../utilitarios/utilitarios.hpp"
#include "../buscar/buscar.hpp"
#include "../buscar_v2/buscar_v2.hpp"
#include "../buscar_v3/buscar_v3.hpp"
using namespace std;

int contador=0;
int compararCont=0;
int opcionAdmin;
string idComprador;
vector<vector<string> > comparadosX;

void logo1() {
	linea();
	cout<<"     EEEEEEEEEE         XXXX     XXXX"<<endl
		<<"     EEE                 XXXX   XXXX      ----------------------------------- "<<endl
		<<"     EEE         EEEXXX    XXXXXXX        |          E L E C T R O          | "<<endl
		<<"     EEEEEEEEEE  EEEXXX     XXXXX         |           X P R E S S           | "<<endl
		<<"     EEE         EEEXXX    XXXXXXX        |     Busca, Compra, Descubre     | "<<endl
		<<"     EEE                 XXXX   XXXX      ----------------------------------- "<<endl
		<<"     EEEEEEEEEE         XXXX     XXXX"<<endl;;
	linea();
}

void logo2() {
	linea();
	cout<<"        EEEEEEEEEE  EEEEEEEEEE"<<endl
		<<"        EEE         EEE              ----------------------------------- "<<endl
		<<"        EEE         EEE              |          E L E C T R O          | "<<endl
		<<"        EEEEEEEEEE  EEEEEEEEEE       |          E X P R E S S          | "<<endl
		<<"        EEE         EEE              |     Busca, Compra, Descubre     | "<<endl
		<<"        EEE         EEE              ----------------------------------- "<<endl
		<<"        EEEEEEEEEE  EEEEEEEEEE "<<endl;;
	linea();
}

int longitudArchivo(string ubicacion) {
	ifstream archivo;
	int contador=0;
	archivo.open(ubicacion.c_str());
	if (!archivo.is_open()) {
		error();
	}
	else {
		string linea1;
		while (getline(archivo,linea1)) {
			contador++;
		}
	}
	archivo.close();
	return contador;
}

int logeo(string ubicacion) {
	mostrarTile("Login (ID y PSW)");
	string usuario,password;
	if (contador!=3) {
		cout<<"ID: ";cin>>usuario;
		cout<<"PSW: ";cin>>password;
		linea();
		mostrarOp(1,"Logear/Ingresar");
		mostrarOp(2,"Salir"); linea();
		int opcion;
		introducir(opcion,2);
		if (opcion==1) {
			ifstream archivo;
			archivo.open(ubicacion.c_str());
			if (archivo.is_open()==false) {
				error();
			}
			else {
				string linea1;
				int confirmacion=0;
				int longUsuarios=longitudArchivo(ubicacion);
				for (int i=0;i<longUsuarios;i++) {
					getline(archivo,linea1);
					stringstream aux1(linea1);
					string campo1,campo2;
					getline(aux1,campo1,',');
					getline(aux1,campo2,',');
					if (campo1==usuario && campo2==password) {
						i=longUsuarios-1;
						confirmacion=1;
					}
				}
				if (confirmacion==0) {
					cout<<"Su ID y/o Password son Incorrectos"<<endl; linea();
					contador++;
					logeo(ubicacion);
				}
				else if (confirmacion==1){
					idComprador=usuario;
					cout<<"Logeando..."<<endl; linea();
					return 1;
				}
			}
			archivo.close();
		}
		else {
			system("exit");
			return 0;
		}
	}
	else {
		cout<<"Ha alcanzado el limite de ID/PSW incorrectos, Saliendo..."<<endl;
		system("exit");
		return 0;
	}
	return 0;
}

int verificarExistente(vector<vector<string> > vect,int pos,string cadena) {
	int longVectorP=vect.size();
	for (int i=0;i<longVectorP;i++) {
		if (vect[i][pos]==cadena) {
			return 1;
		}
	}
	return 0;
}

void registroCliente() {
	ifstream archivo;
	archivo.open(datosUsuarios.c_str());
	if (archivo.is_open()==false) {
		error();
	}
	else {
		mostrarTile("Registro");
		vector<vector<string> >usuariosRegistrados;
		string linea1;
		while(getline(archivo,linea1)) {
			stringstream aux1(linea1);
			string linea2;
			vector<string> auxiliar;
			while (getline(aux1,linea2,',')) {
				auxiliar.push_back(linea2);
			}
			usuariosRegistrados.push_back(auxiliar);
		}
		string usuarioNuevo; string variable,variable2,variable3,variable4,variable5,variable6,variable7;
		cout<<"ID:"; cin>>variable;
		while (verificarExistente(usuariosRegistrados,0,variable)==1) {
			cout<<"ID ya existe, Ingrese otra: "; cin>>variable;
		}
		usuarioNuevo=variable+",";
		cin.clear();
		cout<<"Password: "; cin>>variable2; usuarioNuevo+=variable2+",";
		cout<<"Nombre: "; cin>>variable3; usuarioNuevo+=variable3+" ";
		cout<<"Apellido: "; cin>>variable4; usuarioNuevo+=variable4+",";
		cin.clear();
		cout<<"DNI: "; cin>>variable5;
		while (STRINGtoINT(variable5)<10000000 && STRINGtoINT(variable5)>99999999) {
			cout<<"Ingrese correctamente (8 cifras): ";cin>>variable5;
		}
		usuarioNuevo+=variable5+",";
		cout<<"Email: "; cin>>variable6; usuarioNuevo+=variable6;
		cout<<"Direccion: "; cin>>variable7; usuarioNuevo+=variable7;
		cout<<usuarioNuevo<<endl;
		ofstream archivo1;
		archivo1.open(datosUsuarios.c_str(),ios::app|ios::out);
		if (archivo1.is_open()==false) {
			error();
		}
		else {
			archivo1<<usuarioNuevo<<endl;
		}
		archivo1.close();
	}
	archivo.close();
}

void pedir1(int &opcion) {
	cout<<"          Ingrese 1 para Continuar y cualquier otro valor para salir: ";
	cin>>opcion;
}

//CLIENTE--------------------------------------------------------------------------
void revisarCarrito() {	//FALTA COMPLETAR
	ifstream archivo; string linea1;
	archivo.open(datosCarritoTemporal.c_str());
	while (getline(archivo,linea1)) {

	}
}

void ajustesCliente(string idComp,int pos,int opc) {
	ifstream archivo1;
	string aux;
	archivo1.open(datosUsuarios.c_str());
	if (archivo1.is_open()==false) {
		error();
	}
	else {
		vector<vector<string> > vectorUsuarios;
		while (getline(archivo1,aux)) {
			vector<string> auxiliar;
			stringstream aux1(aux); string linea1;
			while (getline(aux1,linea1,',')) {
				auxiliar.push_back(linea1);
			}
			vectorUsuarios.push_back(auxiliar);
		}
		int longitud=vectorUsuarios.size();
		int lineaUsuario;
		for (int i=0;i<longitud;i++) {
			if (idComp==vectorUsuarios[i][0]) {
				lineaUsuario=i;
				i=longitud-1;
			}
		}
		string predeterminado;
		if (opc==1) {
			cout<<"Introduzca el valor actual: "; cin>>predeterminado;
			while (predeterminado!=vectorUsuarios[lineaUsuario][pos-1]) {
				cout<<"Incorrecto, introduzca el valor actual: ";cin>>predeterminado;
			}
			cout<<"Introduzca el nuevo valor: ";cin>>predeterminado;
		}
		else if (opc==2) {
			cout<<"Introduzca el nuevo valor: "; cin>>predeterminado;
		}
		else if (opc==3) {
			int valor;
			cout<<"Introduzca el nuevo valor: "; cin>>valor;
			while (valor>99999999 && valor<10000000) {
				cout<<"Erroneo, introduzca el nuevo valor (8 cifras): ";cin>>valor;
			}
			predeterminado=INTtoSTRING(valor);
		}
		vectorUsuarios[lineaUsuario][pos-1]=predeterminado; linea();
		mostrarTile("Confirmacion");
		mostrarOp(1,"Si");
		mostrarOp(2,"No"); linea();
		int opcion;
		introducir(opcion,2);
		if (opcion==1) {
			ofstream archivo;
			archivo.open(datosUsuarios.c_str());
			if (archivo.is_open()==false) {
				error();
			}
			else {
				int longitudLinea=vectorUsuarios[lineaUsuario].size();
				for (int i=0;i<longitud;i++) {
					for (int j=0;j<longitudLinea;j++) {
						archivo<<vectorUsuarios[i][j];
						if (j!=longitudLinea-1) {
							archivo<<",";
						}
					}
					archivo<<endl;
				}
			}
			archivo.close();
		}
	}
	archivo1.close();
}

void revisarHistorial() {
	ifstream archivo;
	archivo.open(datosHistorial.c_str());
	if (archivo.is_open()==false) {
		error();
	}
	else {
		string linea1;
		vector<vector<string> >coincidenciasCarrito;
		while (getline(archivo,linea1)) {
			stringstream aux1(linea1); string linea2;
			getline(aux1,linea2,',');
			if (idComprador==linea2) {
				vector<string> auxiliar;
				auxiliar.push_back(linea2);
				while (getline(aux1,linea2,',')) {
					auxiliar.push_back(linea2);
				}
				coincidenciasCarrito.push_back(auxiliar);
			}
		}
		int longitud=coincidenciasCarrito.size();
		if (longitud==0) {
			cout<<"Parece que aun no tienes historial, que esperas para llenarlo!"<<endl;
			system("pause");
		}
		else {
			cout<<completarCad("",2)
				<<completarCad("Marca",10)
				<<completarCad("Modelo",15)
				<<completarCad("Precio",8)
				<<completarCad("Stock",8)
				<<completarCad("RAM",5)
				<<completarCad("ROM",5)
				<<completarCad("Procesador",12)
				<<completarCad("Color",8)<<endl;
			linea();
			for (int i=0;i<longitud;i++) {
				cout<<INTtoSTRING(i+1)+". "
					<<completarCad(coincidenciasCarrito[i][0],10)
					<<completarCad(coincidenciasCarrito[i][1],15)
					<<completarCad(coincidenciasCarrito[i][2],8)
					<<completarCad(coincidenciasCarrito[i][3],8)
					<<completarCad(coincidenciasCarrito[i][4],5)
					<<completarCad(coincidenciasCarrito[i][5],5)
					<<completarCad(coincidenciasCarrito[i][6],12)
					<<completarCad(coincidenciasCarrito[i][7],8)<<endl;
			}
			system("pause");
		}
	}
}

//ADMIN----------------------------------------------------------------------------
void ajustesAdmin(int pos) {
	ifstream archivo1;
	archivo1.open(datosAdmin.c_str());
	if (archivo1.is_open()==false) {
		error();
	}
	else {
		string linea1,lineaAux="\0",predeterminado;
		for (int i=1;i<=3;i++) {
			getline(archivo1,linea1,',');
			if (i==pos) {
				cout<<"Introduzca el valor actual: ";cin>>predeterminado;
				while (predeterminado!=linea1) {
					cout<<"Incorrecto, introduzca el valor actual: ";cin>>predeterminado;
				}
				cout<<"Introduzca el nuevo valor: ";cin>>predeterminado;
				linea();
				lineaAux+=predeterminado;
			}
			else {
				lineaAux+=linea1;
			}
			if (i!=3) {
				lineaAux+=",";
			}
		}
		cout<<lineaAux<<endl;
		ofstream archivo2;
		archivo2.open(datosAdmin.c_str(),ios::trunc);
		if (archivo2.is_open()==false) {
			error();
		}
		else {
			archivo2<<lineaAux;
			cout<<"Cambiado con Exito"<<endl;
			system("pause");
		}
		archivo2.close();
	}
	archivo1.close();
}

void buscarProducto(int modo) {	//2 para mostrar detalles | 1 para procesar
	ifstream archivo;
	archivo.open(datosCelulares.c_str());
	if (archivo.is_open()==false) {
		error();
	}
	else {
		string linea1;
		vector<vector<string> > vectorCelulares;
		while (getline(archivo,linea1)) {
			stringstream aux(linea1); string linea2;
			vector<string> auxiliar;
			while (getline(aux,linea2,',')) {
				auxiliar.push_back(linea2);
			}
			vectorCelulares.push_back(auxiliar);
		}
		int longP=vectorCelulares.size();
		int longS=vectorCelulares[0].size();
		vector<int> posiciones;

		mostrarTile("Buscar por Nombre");
		string nombre;
		cout<<"Ingrese el nombre del celular: "; cin>>nombre;
		//int longitudBuscar=nombre.length();
		for (int i=0;i<longP;i++) {
			//int longitudCadena=vectorCelulares[i][0].length();
			int posicion=0; int existencia,existencia2;
			existencia=vectorCelulares[i][0].find(nombre);
			existencia2=vectorCelulares[i][1].find(nombre);
			if (existencia==-1 && existencia2==-1) {

			}
			else {
				posicion=i;
				posiciones.push_back(posicion);
			}
		}
		mostrarTile("Resultados");
		int longResultados=posiciones.size();
		if (longResultados==0) {
			cout<<"No se encontro coincidencias"<<endl;
		}
		else {
			cout<<completarCad("",2)
				<<completarCad("Marca",10)
				<<completarCad("Modelo",15)
				<<completarCad("Precio",8)
				<<completarCad("Stock",8)
				<<completarCad("RAM",5)
				<<completarCad("ROM",5)
				<<completarCad("Procesador",12)
				<<completarCad("Color",8)<<endl;
			linea();
			for (int i=0;i<longResultados;i++) {
				cout<<INTtoSTRING(i+1)+". "
					<<completarCad(vectorCelulares[posiciones[i]][0],10)
					<<completarCad(vectorCelulares[posiciones[i]][1],15)
					<<completarCad(vectorCelulares[posiciones[i]][2],8)
					<<completarCad(vectorCelulares[posiciones[i]][3],8)
					<<completarCad(vectorCelulares[posiciones[i]][4],5)
					<<completarCad(vectorCelulares[posiciones[i]][5],5)
					<<completarCad(vectorCelulares[posiciones[i]][6],12)
					<<completarCad(vectorCelulares[posiciones[i]][7],8)<<endl;
			}
			menuAnterior3(longResultados+1);
			int opcion;
			introducir(opcion,longResultados);
			if (opcion!=longResultados) {
				if (modo==1) {	//
					mostrarTile(vectorCelulares[posiciones[opcion-1]][0]+","+
								vectorCelulares[posiciones[opcion-1]][1]+","+
								vectorCelulares[posiciones[opcion-1]][2]+","+
								vectorCelulares[posiciones[opcion-1]][3]+","+
								vectorCelulares[posiciones[opcion-1]][4]+","+
								vectorCelulares[posiciones[opcion-1]][5]+","+
								vectorCelulares[posiciones[opcion-1]][6]+","+
								vectorCelulares[posiciones[opcion-1]][7]);
					mostrarOp(1,"Cambiar la cantidad en Stock");
					mostrarOp(2,"Eliminar de la lista de celulares");
					menuAnterior3(3); int opcion2;
					introducir(opcion2,3);
					if (opcion2==1) {
						cout<<"Stock Actual: "<<vectorCelulares[posiciones[opcion-1]][3]<<endl;
						int nuevo;
						cout<<"Stock Nuevo: "; cin>>nuevo;
						vectorCelulares[posiciones[opcion-1]][3]=INTtoSTRING(nuevo);
					}
					else if (opcion2==2) {
						mostrarTile("Confirmacion");
						mostrarOp(1,"Si");
						mostrarOp(2,"No");
						int opcion3; introducir(opcion3,2);
						if (opcion3==1) {
							vector<vector<string> >::iterator posVector;
							posVector=vectorCelulares.begin()+opcion-2;
							vectorCelulares.erase(posVector);
							longP=vectorCelulares.size();
						}
					}
					//PROCESO DE ESCRITURA
					ofstream archivo2;
					archivo2.open(datosCelulares.c_str(),ios::trunc|ios::out);
					if (archivo2.is_open()==false) {
						error();
					}
					else {
						for (int i=0;i<longP;i++) {
							for (int j=0;j<longS;j++) {
								archivo2<<vectorCelulares[i][j];
								if (j!=longS-1) {
									archivo2<<",";
								}
								else {
									archivo2<<endl;
								}
							}
						}
						cout<<"Cambios Realizados con Exito"<<endl;
					}
					archivo2.close();
				}
				else if (modo==2) {
					ifstream archivo2;
					archivo2.open(datosFiltros.c_str());
					if (archivo2.is_open()==false) {
						error();
					}
					else {
						string linea1;
						vector<string> filtrosX;
						while (getline(archivo2,linea1)) {
							stringstream aux1(linea1);
							string linea2;
							getline(aux1,linea2,',');
							filtrosX.push_back(linea2);
						}
						int longitudFiltros=filtrosX.size();
						for (int i=0;i<longitudFiltros;i++) {
							switch (i) {
								case 0: cout<<"Informacion------------------"<<endl;
									break;
								case 4: cout<<"Componente-------------------"<<endl;
									break;
								case 7: cout<<"Apariencia-------------------"<<endl;
									break;
								case 12: cout<<"Bateria---------------------"<<endl;
									break;
								case 15: cout<<"Camara----------------------"<<endl;
									break;
								case 19: cout<<"Conectividad----------------"<<endl;
									break;
								case 21: cout<<"Pantalla--------------------"<<endl;
									break;
								case 24: cout<<"Seguridad-------------------"<<endl;
									break;
								case 27: cout<<"Software--------------------"<<endl;
									break;
								case 28: cout<<"Extras----------------------"<<endl;
									break;
							}
							cout<<"   "<<filtrosX[i]<<": "<<vectorCelulares[opcion-1][i]<<endl;
						}
						system("pause");
						mostrarTile("Acciones");
						mostrarOp(1,"Aniadirlo al Carro");
						mostrarOp(2,"Compararlo");
						menuAnterior3(3);
						int opcion3;
						introducir(opcion3,3);
						switch (opcion3) {
							case 1:
								break;
							case 2: if (compararCont==1) {
										for (int i=0;i<longitudFiltros;i++) {
											switch (i) {
												case 0: cout<<"Informacion--------------------------------------------"<<endl;
													break;
												case 4: cout<<"Componente---------------------------------------------"<<endl;
													break;
												case 7: cout<<"Apariencia---------------------------------------------"<<endl;
													break;
												case 12: cout<<"Bateria-----------------------------------------------"<<endl;
													break;
												case 15: cout<<"Camara------------------------------------------------"<<endl;
													break;
												case 19: cout<<"Conectividad------------------------------------------"<<endl;
													break;
												case 21: cout<<"Pantalla----------------------------------------------"<<endl;
													break;
												case 24: cout<<"Seguridad---------------------------------------------"<<endl;
													break;
												case 27: cout<<"Software----------------------------------------------"<<endl;
													break;
												case 28: cout<<"Extras------------------------------------------------"<<endl;
													break;
											}
											cout<<"   "<<completarCad(filtrosX[i]+": ",20)<<completarCad(comparadosX[0][i],10)<<completarCad(comparadosX[1][i],10)<<endl;
										}
										linea();
										mostrarOp(1,"Comprar "+comparadosX[0][0]+" "+comparadosX[0][1]);
										mostrarOp(2,"Comprar "+comparadosX[1][0]+" "+comparadosX[1][1]);
										menuAnterior3(3);
										int opcion4;
										introducir(opcion4,3);
										if (opcion4!=3) {
											ofstream archivo2;
											archivo2<<idComprador;
											archivo2.open(datosCarritoTemporal.c_str(),ios::app|ios::out);
											for (int k=0;k<longS;k++) {
												archivo2<<comparadosX[opcion4-1][k];
												if (k!=longS-1) {
													archivo2<<",";
												}
												else {
													archivo2<<endl;
												}
											}
											cout<<"Aniadido con Exito al Carrito"<<endl;
											archivo2.close();
											system("pause");
										}
										compararCont=0;
									}
									if (compararCont==0) {
										comparadosX.push_back(vectorCelulares[opcion-1]);
										buscarProducto(modo);
										compararCont++;
									}
								break;
						}
					}
					archivo2.close();
				}
			}
		}
	}
	archivo.close();
}

//FUNCIONES EN EL MAIN-------------------------------------------------------------
void menuCliente() {
	int opcion2;
	mostrarTile("Bienvenido <"+idComprador+">");
	mostrarOp(1,"Smartphones Recomendados");
	mostrarOp(2,"Buscar y Comparar");
	mostrarOp(3,"Revisar Carrito");
	mostrarOp(4,"Revisar Historial");
	mostrarOp(5,"Ajustes de Cuenta");
	mostrarOp(6,"Salir");
	linea();
	introducir(opcion2,6);
	switch (opcion2) {
		case 1: buscarRecomendados(idComprador);
			break;
		case 2: mostrarTile("Buscar");
				mostrarOp(1,"Por Nombre");
				mostrarOp(2,"Por Filtros");
				menuAnterior3(3); int opcion4;
				introducir(opcion4,3);
				switch (opcion4) {
					case 1: buscarProducto(2);
						break;
					case 2: buscar3(idComprador,0);
						break;
				}
			break;
		case 3:
			break;
		case 4: revisarHistorial();
			break;
		case 5: mostrarTile("Ajustes de Cuenta");
				mostrarOp(1,"Cambiar ID");
				mostrarOp(2,"Cambiar Password");
				mostrarOp(3,"Cambiar Nombre");
				mostrarOp(4,"Cambiar # de TLF");
				mostrarOp(5,"Cambiar de DNI");
				mostrarOp(6,"Cambiar de Email");
				mostrarOp(7,"Cambiar de Direccion");
				menuAnterior3(8); int opcion;
				introducir(opcion,8);
				switch (opcion) {
					case 1: ajustesCliente(idComprador,1,1);
						break;
					case 2: ajustesCliente(idComprador,2,1);
						break;
					case 3: ajustesCliente(idComprador,3,2);
						break;
					case 4: ajustesCliente(idComprador,4,2);
						break;
					case 5: ajustesCliente(idComprador,5,3);
						break;
					case 6: ajustesCliente(idComprador,6,2);
						break;
					case 7: ajustesCliente(idComprador,7,2);
						break;
				}
			break;
		case 6: system("exit");
			break;
	}
	menuCliente();
}

void menuAdmin() {
	int opcion2;
	mostrarTile("Bienvenido <"+idComprador+">");
	mostrarOp(1,"Buscar Celulares");
	mostrarOp(2,"Acceder a Lista de Usuarios");
	mostrarOp(3,"Modificar datos de los Smartphones");
	mostrarOp(4,"Ajustes de Cuenta");
	mostrarOp(5,"Salir");
	linea();
	introducir(opcion2,5);
	switch (opcion2) {
		case 1: buscarProducto(2);
			break;
		case 2:
			break;
		case 3:
			break;
		case 4: mostrarTile("Ajustes de Cuenta");
				mostrarOp(1,"Cambiar ID");
				mostrarOp(2,"Cambiar Password");
				mostrarOp(3,"Cambiar # de Opcion Admin");
				menuAnterior3(4); int opcion;
				introducir(opcion,4);
				switch (opcion) {
					case 1: ajustesAdmin(1);
						break;
					case 2: ajustesAdmin(2);
						break;
					case 3: ajustesAdmin(3);
						break;
				}
			break;
		case 5: system("exit");
			break;
	}
	menuAdmin();
}

void limpiarTXT(string ubicacion) {
	ofstream archivo;
	archivo.open(ubicacion.c_str(),ios::out|ios::trunc);
	if (archivo.is_open()==false) {
		error();
	}
	archivo.close();
}

void menuPrincipal() {
	limpiarTXT(datosCarritoTemporal);
	ifstream archivoAdmin;
	archivoAdmin.open(datosAdmin.c_str());
	if (archivoAdmin.is_open()==false) {
		error();
	}
	else {
		string lineaAdmin;
		getline(archivoAdmin,lineaAdmin,',');
		getline(archivoAdmin,lineaAdmin,',');
		getline(archivoAdmin,lineaAdmin,',');
		opcionAdmin=STRINGtoINT(lineaAdmin);
	}
	logo2();
	int opcion,auxiliarBool;
	pedir1(opcion);
	linea();
	if (opcion==0) {
		mostrarTile("Saliendo...");
		system("exit");
	}
	else {
		if (opcion==1) {
			mostrarTile("Login al Sistema");
			mostrarOp(1,"Logear");
			mostrarOp(2,"Registrar");
			menuAnterior3(3);
			int opcion2;
			introducir(opcion2,4);
			if (opcion2==1) {
				auxiliarBool=logeo("datos/usuarios.txt");
				if (auxiliarBool==1) {
					menuCliente();
				}
			}
			else if (opcion2==2) {
				registroCliente();
				menuPrincipal();
			}
			else if (opcion2==3) {
				system("exit");
			}
		}
		else if (opcion==opcionAdmin) {
			auxiliarBool=0;
			mostrarTile("Login Admin");
			while (auxiliarBool==0) {
				auxiliarBool=logeo("datos/admin.txt");
			}
			if (auxiliarBool==1) {
				menuAdmin();
			}
		}
	}
	limpiarTXT(datosCarritoTemporal);
}
