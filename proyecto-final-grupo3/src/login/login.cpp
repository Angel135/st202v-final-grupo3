/*
 * login.cpp
 *
 *  Created on: 2 jun. 2018
 *      Author: User
 */

#include <string>
#include <iostream>
#include "../utilitarios/utilitarios.hpp"
#include "../lobby/lobby.hpp"
#include "../admin/admin.hpp"
#include "../login/login.hpp"
#include "../vendedor/vendedor.hpp"
void login();
using namespace std;

/*
void login() {
	int opcion=0;
	cout<< "               LOGIN" << endl;
	linea();
	//cout<<"ID: "; cin>>id;
	//cout<<"PSW: "; cin>>psw;
	linea();
	cout<< "Logeando..." << endl;
	linea();
	system("cls");
	cout<<"          BIENVENIDO JUAN"<<endl;
	linea();
	cout<<"1. Recomendados"<<endl;
	cout<<"2. Buscar"<<endl;
	cout<<"3. Comparar"<<endl;
	cout<<"4. Cerrar Sesion"<<endl;
	cout<<"5. Cerrar E-X"<<endl;
	linea();
	cout<<"Ingresar Opcion: "; cin>>opcion;
	linea();
	system("cls");
	switch (opcion) {
		case 1:	cout<<"             RECOMENDADOS"<<endl;
				linea();
				cout<<"1. Apartado fotografico"<<endl;
				cout<<"2. Potencia del movil"<<endl;
				cout<<"3. Baterias duraderas"<<endl;
				cout<<"4. Budget Phone"<<endl;
				linea();
				cout<<"Ingresar Opcion: "; cin>>opcion;
				linea();
				system("cls");
				//resultados();
			break;
		case 2:	cout<<"                BUSCAR"<<endl;
				linea();
				cout<<"1. Por nombre y filtros"<<endl;
				cout<<"2. Por filtros"<<endl;
				linea();
				cout<<"Ingresar Opcion: "; cin>>opcion;
				linea();
				system("cls");
				switch (opcion) {
					case 1:	cout<<"              POR NOMBRE"<<endl;
							linea();
							//cout<<"Ingresar nombre: "; cin>>nom_comp;
							linea();
							system("cls");
							//resultados();
						break;
					case 2:	//buscar_filtros();
						break;
				}
			break;
		case 3:	cout<<"Under Development..."<<endl;
			break;
		case 4:	login();
			break;
		case 5:	cout<<"Cerrando..."<<endl;
				linea();
				system("exit");
			break;
		default:
			cout<<"Error"<<endl;
	}
}
*/
struct usuario{
	string nombre;
	string dni;
	string nombre_usuario;
	string contra;
	string telefono;
	string email;
};

void llenar_guardar( string nombre, string dni,string nombreusuario,string contrasenia,string numero_telefono,string email,int tipo){
 struct usuario nuevo;
 nuevo.nombre=nombre;
 nuevo.dni=dni;
 nuevo.nombre_usuario=nombreusuario;
nuevo.contra=contrasenia;
nuevo.telefono=numero_telefono;
nuevo.email=email;

ofstream arc;
if(tipo==1){
arc.open("datos/admin.txt",ios::app);
if (arc.is_open()){
arc<<nuevo.nombre;
arc<<" ";
arc<<nuevo.dni;
arc<<" ";
arc<<nuevo.nombre_usuario;
arc<<" ";
arc<<nuevo.contra;
arc<<" ";
arc<<nuevo.telefono;
arc<<" ";
arc<<nuevo.email<<endl;
arc.close();
}
else{
	cout<<"Error al guardar"<<endl;
}
}
if(tipo==2){
	arc.open("datos/usuarios.txt",ios::app);
	if (arc.is_open()){
		arc<<nuevo.nombre;
		arc<<" ";
		arc<<nuevo.dni;
		arc<<" ";
		arc<<nuevo.nombre_usuario;
		arc<<" ";
		arc<<nuevo.contra;
		arc<<" ";
		arc<<nuevo.telefono;
		arc<<" ";
		arc<<nuevo.email<<endl;
	arc.close();
	}
	else{
		cout<<"Error al guardar"<<endl;
	}
}
if(tipo==3){
	arc.open("datos/vendedor.txt",ios::app);
		if (arc.is_open()){
			arc<<nuevo.nombre;
			arc<<" ";
			arc<<nuevo.dni;
			arc<<" ";
			arc<<nuevo.nombre_usuario;
			arc<<" ";
			arc<<nuevo.contra;
			arc<<" ";
			arc<<nuevo.telefono;
			arc<<" ";
			arc<<nuevo.email<<endl;
		arc.close();
		}
		else{
			cout<<"Error al guardar"<<endl;
		}
}


}
void registro() {
	int opregistro=0;
	string nombre,dni,contrasenia1,contrasenia2,nombre_usuario,telefono,email;
	int tipo;
	mostrarTile("*****REGISTRO*****");
	linea();
		cin.clear();
		mostrarTile("Desea continuar");
		mostrarOp(1,"Si");
		mostrarOp(2,"No");
		cin>>opregistro;
		switch(opregistro){
		case 1:
				mostrarTile("Por favor, complete los siguientes campos(escriba un solo nombre por favor");

				mostrarTile("Tipo de usuario");
				mostrarOp(1,"1.-Administrador");

				mostrarOp(2,"2.-Cliente");
				mostrarOp(3,"3.-Vendedor");
				cin>>tipo;
				cin.ignore();
				mostrarTile("Nombre: ");
				getline(cin,nombre);
				mostrarTile("DNI: ");
				getline(cin,dni);
				mostrarTile("Nombre de usuario: ");
				getline(cin,nombre_usuario);
				mostrarTile("Contrasenia: ");
				getline(cin,contrasenia1);
				mostrarTile("Confirmar contrasenia: ");
				getline(cin,contrasenia2);
				bool vale;
				while(vale==false){
					if(contrasenia1==contrasenia2){
						vale=true;
					}
					else{
						cout<<"Las contraseņas no coinciden"<<endl;
						mostrarTile("Contrasenia: ");
										getline(cin,contrasenia1);
										mostrarTile("Confirmar contrasenia: ");
										getline(cin,contrasenia2);
					}
				}
				mostrarTile("Numero de TLF: ");
				getline(cin,telefono);
				mostrarTile("Email: ");
				getline(cin,email);
				linea();
				//cout<<"      PASSWORD GENERADO: awdrg123"<<endl;
				linea();
				llenar_guardar(nombre,dni,nombre_usuario,contrasenia1,telefono,email,tipo);
  if(tipo==1){
      menuinicio();
  }
  if(tipo==3){
	  menuVendedor();
  }
				break;
		case 2: login();
			break;
		}
	system("pause");
}
bool validar(string usuario, string contrasenia,int tipo){
	string nombre,dni,nombre_usuario,contra;
	vector<struct usuario>administradores;
	vector<struct usuario>usuarios;
	vector<struct usuario>vendedores;
	bool encontrado=false;
if(tipo==1){
	ifstream arc;
	arc.open("datos/admin.txt");
	if(arc.is_open()){
		struct usuario admi;
		while(!arc.eof()){
			arc>>admi.nombre;
			arc>>admi.dni;
			arc>>admi.nombre_usuario;
			arc>>admi.contra;

			arc>>admi.telefono;
			arc>>admi.email;


			administradores.push_back(admi);
		}
		int n=administradores.size();
		for(int i=0;i<n;i++){
			if(administradores[i].nombre_usuario==usuario && administradores[i].contra==contrasenia){
				encontrado=true;
				break;
			}

		}
		arc.close();
	}
	else{
		cout<<"Error al abrir el archivo"<<endl;
	}
}
if(tipo==2){
	ifstream arc;
		arc.open("datos/usuarios.txt");
		if(arc.is_open()){
			struct usuario user;
			while(!arc.eof()){
				arc>>user.nombre;
				arc>>user.dni;
				arc>>user.nombre_usuario;
				arc>>user.contra;

				arc>>user.telefono;
				arc>>user.email;
				usuarios.push_back(user);
			}
			int n=usuarios.size();
			for(int i=0;i<n;i++){
				if(usuarios[i].nombre_usuario==usuario && usuarios[i].contra==contrasenia){
					encontrado=true;
					break;
				}
			}
			arc.close();
		}
		else{
			cout<<"Error al abrir el archivo"<<endl;
		}
}
if(tipo==3){
	ifstream arc;
			arc.open("datos/vendedor.txt");
			if(arc.is_open()){
				struct usuario vendedor;
				while(!arc.eof()){
					arc>>vendedor.nombre;
					arc>>vendedor.dni;
					arc>>vendedor.nombre_usuario;
					arc>>vendedor.contra;

					arc>>vendedor.telefono;
					arc>>vendedor.email;
					vendedores.push_back(vendedor);
				}
				int n=vendedores.size();
				for(int i=0;i<n;i++){
					if(vendedores[i].nombre_usuario==usuario && vendedores[i].contra==contrasenia){
						encontrado=true;
						break;
					}
				}
				arc.close();
			}
			else{
				cout<<"Error al abrir el archivo"<<endl;
			}
}
return encontrado;
}


void log_in(){
	int oplog_in=0;
	int tipo;
	string nombre,contrasenia;
	mostrarTile("Desea continuar");
		mostrarOp(1,"Si");
		mostrarOp(2,"No");
		cin>>oplog_in;
		switch(oplog_in){
			case 1:
				mostrarTile("*****LOGIN*****");
				mostrarTile("Por favor, complete los siguientes campos para acceder a su cuenta");
				mostrarTile("Tipo de usuario");
				cout<<"1.Administrado"<<endl;
				cout<<"2.Cliente"<<endl;
				cout<<"3.Vendedor"<<endl;
				cin>>tipo;
				cin.ignore();
				mostrarTile("Nombre de usuario");
				getline(cin,nombre);
				mostrarTile("Contrasenia");
				getline(cin,contrasenia);
				mostrarTile("No recuerdo mi contrasenia");
				if(validar(nombre,contrasenia,tipo)){
					if(tipo==1){
						menuinicio();

					}
					if(tipo==3){
						menuVendedor();
					}
				}else{
					cout<<"Elusuario y/o contraseņa son incorrectos"<<endl;
				}


				break;
			case 2:
				login();
				break;
		}
}

void login(){
	int opcion=0,oplogin=0;
	cin.clear();
	linea();
	mostrarTile("Desea continuar");
		mostrarOp(1,"Si");
		mostrarOp(2,"No");
		cin>>oplogin;
		switch(oplogin){
			case 1:
				mostrarTile("OPCIONES");
				linea();
				mostrarOp(1,"Ya tengo cuenta");
				mostrarOp(2,"Todavia no tengo cuenta");
				linea();
				mostrarTile("Ingresar Opcion: ");
				cin>>opcion;
				linea();
				cin.clear();
				switch (opcion) {
					case 1: log_in();
					break;
					case 2:	registro();
					break;
					default: login();
				};
				system("cls");
			break;
			case 2: //lobby();
			break;
}
}
