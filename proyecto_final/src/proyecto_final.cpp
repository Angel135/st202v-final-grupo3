#include "utilitarios/utilitarios.hpp"

struct cliente {
	string nombre_completo;
	int DNI;
	string nombre_usuario;
	string contrasenia;
	int verificar_contrasenia;
	string e_mail;
	int telefono;
};

struct celular {
	float precio;
	int sim;
	struct pantalla{
		string tecnologia;
		string resolucion;
		float tamanio;
		int brillo;
		int capacidad_bateria;
	};
	string jack_de_audifonos;
	string puerto_de_entrada;
	string stereo_speaker;
};

struct cliente nuevo;

string nom_comp, id, psw, email;
int dni, telef, opcion=1, op1, op2, op3, op4;

void resultados() {
	cout<<"            RESULTADOS"<<endl;
	linea();
	cout<<" PRODUCTO PRECIO"<<endl;
	cout<<" PROD A PRECIO A"<<endl;
	cout<<" PROD B PRECIO B"<<endl;
	cout<<" PROD C PRECIO C"<<endl;
	cout<<" PROD D PRECIO D"<<endl;
	cout<<" PROD E PRECIO E"<<endl;
	cout<<" PROD F PRECIO F"<<endl;
	cout<<" PROD G PRECIO G"<<endl;
	cout<<" PROD H PRECIO H"<<endl;
	cout<<" PROD I PRECIO I"<<endl;
}

void buscar_filtros() {
	cout<<"              POR FILTROS"<<endl;
	linea();
	cout<<"               POR PRECIO"<<endl;
	linea();
	cout<<"Rango de Precios:"<<endl;
	cout<<"1. <100$"<<endl;
	cout<<"2. 100$-200$"<<endl;
	cout<<"3. 200$-400$"<<endl;
	cout<<"4. 400$>"<<endl;
	linea();
	cout<<"Ingresar Opcion: "; cin>>opcion;
	linea();
	cout<<"                POR SIM"<<endl;
	cout<<"1. 1 SIM"<<endl;
	cout<<"2. 2 SIM"<<endl;
	linea();
	cout<<"Ingresar Opcion: "; cin>>opcion;
	linea();
	cout<<"              POR PANTALLA"<<endl;
	cout<<"1. <=5'"<<endl;
	cout<<"2. >5'"<<endl;
	linea();
	cout<<"Ingresar Opcion: "; cin>>opcion;
	linea();
	cout<<"            PUERTO DE ENTRADA"<<endl;
	cout<<"1. MicroUSB"<<endl;
	cout<<"2. USB tipo C"<<endl;
	cout<<"3. Lightning"<<endl;
	linea();
	cout<<"Ingresar Opcion: "; cin>>opcion;
	linea();
	cout<<"Procesado..."<<endl;
	linea();
	resultados();
}

void login() {
	cout<< "               LOGIN" << endl;
	linea();
	cout<<"ID: "; cin>>id;
	cout<<"PSW: "; cin>>psw;
	linea();
	cout<< "Logeando..." << endl;
	linea();
	system("cls");
	cout<<"          BIENVENIDO JUAN"<<endl;
	linea();
	cout<<"1. Recomendados"<<endl;
	cout<<"2. Buscar"<<endl;
	cout<<"3. Comparar"<<endl;
	cout<<"4. Cerrar Sesion"<<endl;
	cout<<"5. Cerrar E-X"<<endl;
	linea();
	cout<<"Ingresar Opcion: "; cin>>opcion;
	linea();
	system("cls");
	switch (opcion) {
		case 1:	cout<<"             RECOMENDADOS"<<endl;
				linea();
				cout<<"1. Apartado fotografico"<<endl;
				cout<<"2. Potencia del movil"<<endl;
				cout<<"3. Baterias duraderas"<<endl;
				cout<<"4. Budget Phone"<<endl;
				linea();
				cout<<"Ingresar Opcion: "; cin>>opcion;
				linea();
				system("cls");
				resultados();
			break;
		case 2:	cout<<"                BUSCAR"<<endl;
				linea();
				cout<<"1. Por nombre y filtros"<<endl;
				cout<<"2. Por filtros"<<endl;
				linea();
				cout<<"Ingresar Opcion: "; cin>>opcion;
				linea();
				system("cls");
				switch (opcion) {
					case 1:	cout<<"              POR NOMBRE"<<endl;
							linea();
							cout<<"Ingresar nombre: "; cin>>nom_comp;
							linea();
							system("cls");
							resultados();
						break;
					case 2:	buscar_filtros();
						break;
				}
			break;
		case 3:	cout<<"Under Development..."<<endl;
			break;
		case 4:	login();
			break;
		case 5:	cout<<"Cerrando..."<<endl;
				linea();
				system("exit");
			break;
		default:
			cout<<"Error"<<endl;
	}
}

void registro() {
	cout<< "              REGISTRO" << endl;
	linea();
	cout<<"Nombre: "; cin>>nuevo.nombre_completo;
	cout<<"DNI: "; cin>>nuevo.DNI;
	cout<<"ID: "; cin>>nuevo.nombre_usuario;
	cout<<"Numero de TLF: "; cin>>nuevo.telefono;
	cout<<"Email: "; cin>>nuevo.e_mail;
	linea();
	cout<<"      PASSWORD GENERADO: awdrg123"<<endl;
	linea();
	system("pause");
}

void principal() {
	cout<< "           ELECTRO EXPRESS" << endl;
	linea();
	cout<<" Su tienda de confianza en celulares"<<endl;
	cout<<"     Busque , Compare , Descubra"<<endl;
	linea();
	cout<<"              OPCIONES"<<endl;
	linea();
	cout<<"1. Ya tengo cuenta"<<endl;
	cout<<"2. Todavia no tengo cuenta"<<endl;
	linea();
	cout<<"Ingresar Opcion: "; cin>>opcion;
	linea();
	system("cls");
	switch (opcion) {
		case 1: login();
			break;
		case 2:	registro();
				login();
			break;
	};
}

int main() {
	principal();

	return 0;
}


//:vvvvvvv
